import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { ToastController } from 'ionic-angular';
import { Facebook } from '@ionic-native/facebook';

import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, FirebaseObjectObservable } from 'angularfire2/database';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/take';

import * as fromRoot from './../store/app.reducer';
import {
  ILoginCredentials,
  IFirebaseUserProfile,
  IFirebaseBasicProfile,
  IGeolocation
} from './../store/authentication/authentication.types';

declare var google;

@Injectable()
export class AuthService {
  public geolocation$: Observable<IGeolocation>;
  constructor(
    private afAuth: AngularFireAuth,
    private afData: AngularFireDatabase,
    private facebook: Facebook,
    public toastCtrl: ToastController,
    private store: Store<fromRoot.State>
  ) {
    this.geolocation$ = store.select(fromRoot.getGeoLocation)
      .filter(location => location !== null);
  }

  public signUp(userCredentials: ILoginCredentials): Observable<ILoginCredentials> {
    const { email, password, confirmPassword } = userCredentials;
    const registerError: { [key: string]: any } = {
      title: 'Unable to register',
      buttons: ['Ok']
    };

    if (password !== confirmPassword) {
      registerError.message = 'Password and confirmation password do not match.';
      return Observable.throw(registerError);
    }

    const signUpPromise = <Promise<any>>this.afAuth.auth.createUserWithEmailAndPassword(email, password);

    return Observable.fromPromise(signUpPromise)
      .map(() => userCredentials)
      .catch(error => {
        registerError.originalError = error;
        registerError.message = error.message;

        return Observable.throw(registerError);
      });
  }

  public login(loginCredentials: ILoginCredentials): Observable<IFirebaseUserProfile> {
    const { email, password } = loginCredentials;
    const loginPromise = <Promise<any>>this.afAuth.auth.signInWithEmailAndPassword(email, password);
    return Observable.fromPromise(loginPromise)
      .map(this.extractUserProfile.bind(this))
      .catch(error => {
        const loginError: { [key: string]: any } = {
          title: 'Unable to login',
          message: '',
          buttons: ['Ok'],
          originalError: error,
        };

        if (error.code === 'auth/user-not-found') {
          loginError.message = 'No user record corresponding to this email found.';
        } else if (error.code === 'auth/wrong-password') {
          loginError.message = 'Incorrect password. Please try again.';
          loginError.buttons = ['Retry'];
        } else {
          loginError.message = 'Please check that you provided correct credentials, and that your device is connected to the internet.';
        }

        return Observable.throw(loginError);
      });
  }

  public resetPassword(email: string): Observable<any> {
    const resetPasswordPromise = <Promise<any>>this.afAuth.auth.sendPasswordResetEmail(email);
    return Observable.fromPromise(resetPasswordPromise)
      .do(() => {
        const toast = this.toastCtrl.create({
          message: 'Reset password link sent',
          duration: 2000,
          position: 'bottom'
        });

        toast.present();
      })
      .catch(error => {
        const resetPasswordError: { [key: string]: any } = {
          title: 'Unable to Reset Password',
          buttons: ['Ok'],
          originalError: error,
        };

        if (error.code === 'auth/user-not-found') {
          resetPasswordError.message = 'No user record corresponding to this email found.';
        } else if (error.code === 'auth/invalid-email') {
          resetPasswordError.message = 'The e-mail address you have provided is invalid.';
          resetPasswordError.buttons = ['Retry'];
        }

        return Observable.throw(resetPasswordError);
      });
  }

  public facebookAuth(scopes: Array<string>): Observable<IFirebaseUserProfile> {
    const facebookPromise: Promise<any> = this.facebook.login(scopes)
      .then(response => {
        const accessToken = response.authResponse.accessToken;
        const facebookCredential = firebase.auth.FacebookAuthProvider
          .credential(accessToken);

        return this.afAuth.auth.signInWithCredential(facebookCredential);
      })
      .catch(error => {
        const newError = { code: error.code, message: error.message };
        return Promise.reject(newError);
      });

    return Observable.fromPromise(facebookPromise)
      .map(this.extractUserProfile.bind(this))
      .catch(error => {
        let message: string;
        if (error.code === 'auth/account-exists-with-different-credential') {
          message = error.message;
        } else {
          message = 'Unable to authenticate with Facebook. Please try again.';
        }

        const facebookError: { [key: string]: any } = {
          title: 'Authentication Error',
          message,
          buttons: ['Ok'],
          originalError: error
        };

        return Observable.throw(facebookError);
      });
  }

  public logout(): Observable<any> {
    const logoutPromise: Promise<any> = this.facebook.logout()
      .catch(() => 'send to the void!')
      .then(() => <Promise<any>>this.afAuth.auth.signOut());

    return Observable.fromPromise(logoutPromise);
  }

  public updateGeolocation() {
    this.store.select(fromRoot.getAuthUser)
      .combineLatest(this.store.select(fromRoot.getGeoLocation))
      .filter(([user, geolocation]) => (user !== null && geolocation !== null))
      .subscribe(([user, geolocation]: [IFirebaseUserProfile, IGeolocation]) => {
        this.afData.object(`profiles/${user.uid}/geolocation`).set(geolocation);
      });
  }

  public checkAuthState(): Observable<IFirebaseUserProfile> {
    return this.afAuth.authState
      .map(this.extractUserProfile.bind(this))
      .switchMap(data => {
        if (data) {
          return Observable.of(data);
        }
        return Observable.throw('Not logged in');
      });
  }

  private extractUserProfile(data: any): IFirebaseUserProfile {
    if (!data.uid) {
      throw Error('Invalid User Data');
    }

    const providerData: IFirebaseBasicProfile[] = [];
    if (data.providerData.length) {
      data.providerData.forEach(provider => {
        const providerProfile = { ...provider };
        providerData.push(providerProfile);
      });
    }

    const profile: IFirebaseUserProfile = {
      displayName: data.displayName || null,
      email: data.email || null,
      emailVerified: data.emailVerified || false,
      isAnonymous: data.isAnonymous || false,
      photoURL: data.photoURL || null,
      providerData,
      providerId: data.providerId,
      refreshToken: data.refreshToken,
      uid: data.uid
    };

    return profile;
  }
}
