import { AngularFireAuth } from 'angularfire2/auth';
import { async, inject, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { StoreModule, Store } from '@ngrx/store';

import { AngularFireDatabase, AngularFireDatabaseModule } from 'angularfire2/database';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { ProfileService } from './profile.service';
import { AngularFireDatabaseMock } from './../mocks/angularfire2/angular-fire-database.mock';
import { StoreMock } from './../mocks/ngrx/store.mock';
import { State as RootState, reducer, initialState } from './../store/app.reducer';
import * as fromRoot from './../store/app.reducer';
import * as fromAuth from './../store/authentication/authentication.reducer';

describe('ProfileService', () => {
  let profileService: ProfileService;
  let af: AngularFireDatabase;
  let _store: Store<RootState>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AngularFireDatabaseModule,
      ],
      providers: [
        ProfileService,
        {
          provide: AngularFireDatabase,
          useClass: AngularFireDatabaseMock
        },
        {
          provide: Store,
          useValue: new StoreMock(initialState),
        },
      ]
    });
  }));

  beforeEach(() => {
    profileService = TestBed.get(ProfileService);
    af = TestBed.get(AngularFireDatabase);
    _store = TestBed.get(Store);

    AngularFireDatabaseMock.listSpy.calls.reset();
    AngularFireDatabaseMock.pushSpy.calls.reset();
    AngularFireDatabaseMock.updateSpy.calls.reset();
  });

  it('should be created', () => {
    expect(profileService instanceof ProfileService).toBeTruthy();
  });

  it('should have a firebase observable of current user\'s dataLogs', () => {
    const fakeState = { authentication: { user: { uid: 'fakeUID' } } };
    (<any>_store).next(fakeState);

    profileService.getDataLogs();

    expect(af.list).toHaveBeenCalledWith('dataLogs/fakeUID');
  });

  it('should have a firebase observable of current user\'s profile', () => {
    spyOn(af, 'object');
    const fakeState = { authentication: { user: { uid: 'fakeUID' } } };

    (<any>_store).next(fakeState);

    expect(af.object).toHaveBeenCalledWith('profiles/fakeUID');
  });

  describe('updateUnit() on Firebase', () => {
    beforeEach(() => {
      const fakeState = { authentication: { user: { uid: 'valid' } } };
      (<any>_store).next(fakeState);
      spyOn(profileService, 'updateUnit').and.callThrough();
    });

    it('should observe nothing on successful updates', () => {
      const temperatureUnit = 'F';
      const result = profileService.updateUnit(temperatureUnit);

      result.subscribe(response => {
        expect(response).toBeUndefined();
      });

      expect(profileService.profile$.update).toHaveBeenCalledWith({ temperatureUnit });
    });

    it('should observe options for an alert on fail', () => {
      const fakeUpdate = 'invalid';

      const result = profileService.updateUnit('invalid');

      result.subscribe(
        _ => {},
        error => {
          expect(error.title).toBeDefined();
          expect(error.message).toBeDefined();
          expect(error.buttons).toBeDefined();
          expect(error.originalError).toBeDefined();
        }
      );
    });
  });

  describe('getDataLogs() from Firebase', () => {
    beforeEach(() => {
      const fakeState = { authentication: { user: { uid: 'valid' } } };
      (<any>_store).next(fakeState);
      spyOn(profileService, 'getDataLogs').and.callThrough();
    });

    it('should return dataLogs$ firebase observable', () => {

      const result = profileService.getDataLogs();

      expect(profileService.getDataLogs).toHaveBeenCalled();
      expect(result instanceof Observable).toBeTruthy();
    });

    it('should observe an array of datalogs on success', () => {
      const result = profileService.getDataLogs();

      result.subscribe(data => {
        expect(Array.isArray(data)).toBeTruthy();
      });
    });

    it('should observe options for an alert on fail', () => {
      const fakeState = { authentication: { user: { uid: 'invalid' } } };
      (<any>_store).next(fakeState);
      const result = profileService.getDataLogs();

      result.subscribe(
        _ => {},
        error => {
          expect(error.title).toBeDefined();
          expect(error.message).toBeDefined();
          expect(error.buttons).toBeDefined();
          expect(error.originalError).toBeDefined();
        }
      );
    });
  });

  describe('updateDataLogs() on Firebase', () => {
    beforeEach(() => {
      const fakeState = { authentication: { user: { uid: 'valid' } } };
      (<any>_store).next(fakeState);
      spyOn(profileService, 'updateDataLog').and.callThrough();
    });

    it('should observe nothing on successful updates', () => {
      const fakeUpdate = { $key: 'valid', updates: { title: 'fake' } };

      const result = profileService.updateDataLog(fakeUpdate);

      result.subscribe(response => {
        expect(response).toBeUndefined();
      });
      expect(profileService.dataLogs$.update).toHaveBeenCalledWith(fakeUpdate.$key, fakeUpdate.updates);
    });

    it('should observe options for an alert on fail', () => {
      const fakeUpdate = { $key: 'invalid', updates: { title: 'fake' } };

      const result = profileService.updateDataLog(fakeUpdate);

      result.subscribe(
        _ => {},
        error => {
          expect(error.title).toBeDefined();
          expect(error.message).toBeDefined();
          expect(error.buttons).toBeDefined();
          expect(error.originalError).toBeDefined();
        }
      );
      expect(profileService.dataLogs$.update).toHaveBeenCalledWith(fakeUpdate.$key, fakeUpdate.updates);
    });
  });
});
