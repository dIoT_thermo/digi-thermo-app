import { Injectable, Component, NgZone } from '@angular/core';
import { BLE } from '@ionic-native/ble';
import { Diagnostic } from '@ionic-native/diagnostic';
import * as _ from 'lodash';

import { AlertController, AlertOptions, Alert, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';

import { Store } from '@ngrx/store';

import * as fromRoot from '../store/app.reducer';
import * as navigation from '../store/navigation/navigation.actions';
import * as sensor from '../store/sensor/sensor.actions';
import * as authentication from '../store/authentication/authentication.actions';
import { IDeviceReading } from './../store/sensor/sensor.types';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/debounceTime';

@Injectable()
export class BluetoothService {
  connectedDeviceId: string;
  isNotifying = false;
  private bluetoothAlert: Alert;
  public deviceList: Array <any> = [];

  constructor(
    private platform: Platform,
    private ble: BLE,
    private diagnostic: Diagnostic,
    private alertCtrl: AlertController,
    private store: Store<fromRoot.State>,
    private storage: Storage,
    private ngZone: NgZone,
  ) { }

  public isEnabled() {
    const ofBluetooth = this.platform.ready()
      .then(() => Promise.all([
        this.ble.isEnabled(),
        this.storage.get('connectedDeviceId')
      ]))
      .then(([isEnabled, deviceId]) => {
        if (deviceId) {
          this.connectedDeviceId = <any>deviceId;
          return this.ble.isConnected(<any>deviceId)
            .catch(notConnected => {
              return Promise.resolve(false);
            });
        }
        return Promise.resolve(false);
      })
      .then((isConnected) => {
        if (isConnected) {
          this.store.dispatch(new sensor.IsConnectedAction());
        }
        return Promise.resolve();
      });

    return Observable.fromPromise(ofBluetooth)
      .catch(error => {
        return Observable.throw(error);
      });
  }

  public forceBluetooth() {
    if (this.platform.is('android')) {
      this.ble.enable()
        .then(s => {
          this.store.dispatch(new sensor.IsBluetoothEnabledAction());
        })
        .catch(e => {
          this.store.dispatch(new sensor.IsBluetoothEnabledAction());
        });

    } else {
      const subTitle = 'Please check that you have bluetooth turned on. '
        + 'In addition, check that you have enabled DigiThermo to access bluetooth.';
      const options: AlertOptions = {
        title: 'Bluetooth Failed Connection',
        subTitle,
        buttons: [
          {
            text: 'Okay',
          },
          {
            text: 'Go To Settings',
            handler: () => {
              this.diagnostic.switchToSettings();
            },
          },
        ],
      };

      this.bluetoothAlert = this.alertCtrl.create(options);
      this.bluetoothAlert.onWillDismiss(() => {
        this.store.dispatch(new sensor.IsBluetoothEnabledAction());
      });
      this.bluetoothAlert.present();
    }
  }

  public scanDevices() {
    return this.ble.startScan(['AA20'])
      .filter(device => device && device.name === 'HumiTemp Sensor Tag')
      .subscribe((response) => {
        const device = {name: response.name, id: response.id};
        this.ngZone.run(() => {
          this.store.dispatch(new sensor.DeviceFoundAction(device));
        });
      });
  }

  public connectToDevice(device) {
    this.ble.stopScan();
    return this.ble.connect(device.id)
      .subscribe(
        (res) => {
          if (res) {
            const connection = {
              characteristics: res.characteristics,
              id: res.id,
              name: res.name,
              services: res.services,
            };
            this.connectedDeviceId = res.id;
            this.storage.set('connectedDeviceId', res.id);
            this.ngZone.run(() => {
              this.store.dispatch(new sensor.IsConnectedAction());
            });
          } else {
            this.ngZone.run(() => {
              this.store.dispatch(new sensor.IsBluetoothEnabledAction());
            });
          }
      },
      (error) => {
        const alert = this.alertCtrl.create({
          title: 'Oops',
          message: `Unable to connect to sensor ${device.id}, please try again.`,
          buttons: [
            {
              text: 'Ok',
              role: 'cancel',
            }
          ],
        });

        alert.onWillDismiss(() => {
          this.ngZone.run(() => {
            this.store.dispatch(new sensor.ScanDevicesAction());
          });
        });

        alert.present();
      }
    );
  }

  public disconnectFromDevice(device) {
    return this.setSensorEmittingState(0)
      .then(() => this.ble.disconnect(this.connectedDeviceId))
      .then((res) => {
        this.connectedDeviceId = undefined;
        this.isNotifying = false;
        this.store.dispatch(new authentication.LogoutAction());
      });
  }

  public readDevice() {
    if (this.isNotifying) {
      return;
    }

    return this.ble.startNotification(this.connectedDeviceId, 'AA20', 'AA21')
      .debounceTime(1000)
      .subscribe((res) => {
        if (res) {
          this.isNotifying = true;
          const hex = Array.prototype.map.call(new Uint8Array(res), x => ('00' + x.toString(16)).slice(-2)).join('');

          const data = this.hexToDigiThermo(hex);
          this.ngZone.run(() => {
            this.store.dispatch(new sensor.GetSensorDataSuccessAction(data));
          });
        } else {
          // TODO what to do if the reading fails?
          const options: AlertOptions = {
            title: 'Data Reading Failed',
            subTitle: 'Please check your digi thermo device.',
            buttons: [
              {
                text: 'Okay',
                handler: () => {
                  // this.store.dispatch(new thermometer.GetDataFailedAction());
                },
              },
            ],
          };

          this.ngZone.run(() => {
            this.bluetoothAlert = this.alertCtrl.create(options);
            this.bluetoothAlert.present();
          });
        }
      });
  }

  public setSensorEmittingState(value: number) {
    // If we're not setting the state to ON (1) or OFF (0)
    // Then something is not right... so we're not gonna write that
    // value to the sensor and just ignore it for now.
    if (value !== 0 && value !== 1) {
      return;
    }

    const bytes = new Uint8Array(1);
    bytes[0] = value;

    return this.ble.write(this.connectedDeviceId, 'AA20', 'AA22', bytes.buffer)
      .catch(() => 'unable to set state for some reason.');
  }

  public hexToDigiThermo(hex: string): IDeviceReading {
    let placeholder = '';
    let parsedValues = 0;
    let sign;
    let temperatureWhole;
    let temperatureFraction;
    let humidityWhole;
    let humidityFraction;
    for (let i = 0, len = hex.length; i < len; i++) {
      if (placeholder.length % 2 === 0) {
        placeholder = '';
      }
      placeholder += hex[i];
      if (placeholder.length === 2) {
        switch (parsedValues) {
          case 0: // sign
            sign = parseInt(placeholder, 16);
            break;
          case 1: // tempWhole
            temperatureWhole = parseInt(placeholder, 16);
            break;
          case 2: // tempFraction
            temperatureFraction = parseInt(placeholder, 16);
            break;
          case 3: // ignore
            break;
          case 4: // humidWhole
            humidityWhole = parseInt(placeholder, 16);
            break;
          case 5: // humidFraction
            humidityFraction = parseInt(placeholder, 16);
            break;
          default:
            break;
        }
        parsedValues++;
      }
    }
    let temperature = parseFloat(`${temperatureWhole}.${temperatureFraction}`);
    const humidity = parseFloat(`${humidityWhole}.${humidityFraction}`);

    if (sign === 1) {
      temperature = -temperature;
    }
    return { temperature, humidity };
  }

}
