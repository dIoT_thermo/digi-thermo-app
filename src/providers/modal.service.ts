import { Injectable, Component } from '@angular/core';

import { ModalController, Modal } from 'ionic-angular';

import { Store } from '@ngrx/store';

import { FeedbackModalComponent } from './../components/feedback-modal/feedback-modal';
import * as fromRoot from '../store/app.reducer';
import * as navigation from '../store/navigation/navigation.actions';

@Injectable()
export class ModalService {
  public static MODALS = [
    {
      name: 'FeedbackModal',
      component: FeedbackModalComponent
    }
  ];

  private modal: Modal;

  constructor(
    private modalCtrl: ModalController,
    private store: Store<fromRoot.State>
  ) {}

  openModal(byName: string) {
    const modal = ModalService.MODALS.find(m => m.name.toLowerCase() === byName.toLowerCase());

    if (!modal) {
      throw new Error(`Unknown Modal Name: ${byName}`);
    }

    this.modal = this.modalCtrl.create(modal.component);
    this.modal.present();

    this.modal.onDidDismiss(() => {
      this.store.dispatch(new navigation.CloseModalAction(modal.name));
      this.modal = undefined;
    });
  }

  closeModal() {
    if (this.modal) { this.modal.dismiss(); }
  }
}
