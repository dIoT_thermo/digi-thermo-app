import { Facebook } from '@ionic-native/facebook';
import { async, inject, fakeAsync, tick, TestBed } from '@angular/core/testing';

import { Store, StoreModule } from '@ngrx/store';

import { AngularFireAuth, AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireDatabaseModule } from 'angularfire2/database';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { AuthService } from './auth.service';
import { AngularFireAuthMock } from './../mocks/angularfire2/angular-fire-auth.mock';
import { AngularFireDatabaseMock } from './../mocks/angularfire2/angular-fire-database.mock';
import { FacebookMock } from './../mocks/ionic-native/facebook.mock';
import { StoreMock } from './../mocks/ngrx/store.mock';
import { ToastController } from 'ionic-angular';
import { ToastControllerMock } from './../mocks/ionic/toast-controller.mock';
import { State as RootState, reducer, initialState } from './../store/app.reducer';

describe('AuthService', () => {
  let authService: AuthService;
  let afAuth: AngularFireAuth;
  let afData: AngularFireDatabase;
  let facebook: Facebook;
  let toastController: ToastController;
  let _store: Store<RootState>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        AngularFireAuthModule,
        AngularFireDatabaseModule,
        StoreModule.provideStore(reducer),
      ],
      providers: [
        AuthService,
        {
          provide: AngularFireAuth,
          useClass: AngularFireAuthMock
        },
        {
          provide: AngularFireDatabase,
          useClass: AngularFireDatabaseMock
        },
        {
          provide: Facebook,
          useClass: FacebookMock,
        },
        {
          provide: ToastController,
          useClass: ToastControllerMock,
        },
        {
          provide: Store,
          useValue: new StoreMock(initialState),
        }
      ]
    });
  }));

  beforeEach(() => {
    authService = TestBed.get(AuthService);
    afAuth = TestBed.get(AngularFireAuth);
    afData = TestBed.get(AngularFireDatabase);
    facebook = TestBed.get(Facebook);
    toastController = TestBed.get(ToastController);
    _store = TestBed.get(Store);
  });

  it('should be created', () => {
    expect(authService instanceof AuthService).toBeTruthy();
  });

  describe('signUp() using Firebase', () => {
    it('should return user profile on successful signup', () => {
      const credentials = AngularFireAuthMock.VALID_EMAIL_PASSWORD_CONFIRM;
      spyOn(afAuth.auth, 'createUserWithEmailAndPassword').and.callThrough();

      const response = authService.signUp(credentials);

      expect(afAuth.auth.createUserWithEmailAndPassword).toHaveBeenCalledWith(credentials.email, credentials.password);
      response.subscribe(loginCredentials => {
        expect(loginCredentials).toEqual(credentials);
      });
    });

    describe('should return options for an alert if signup fails', () => {
      it('password and confirmPassword does not match', () => {
        const credentials = AngularFireAuthMock.INVALID_EMAIL_PASSWORD_CONFIRM_NO_MATCH;
        spyOn(afAuth.auth, 'createUserWithEmailAndPassword').and.callThrough();

        const response = authService.signUp(credentials);

        expect(afAuth.auth.createUserWithEmailAndPassword).not.toHaveBeenCalled();
        response.subscribe(
          _ => {},
          error => {
            expect(error.title).toBeDefined();
            expect(error.buttons).toBeDefined();
            expect(error.message).toBeDefined();
          }
        );
      });

      it('password and confirmPassword does match', () => {
        const credentials = AngularFireAuthMock.INVALID_EMAIL_PASSWORD_CONFIRM_MATCH;
        spyOn(afAuth.auth, 'createUserWithEmailAndPassword').and.callThrough();

        const response = authService.signUp(credentials);

        expect(afAuth.auth.createUserWithEmailAndPassword).toHaveBeenCalledWith(credentials.email, credentials.password);
        response.subscribe(
          _ => {},
          error => {
            expect(error.title).toBeDefined();
            expect(error.buttons).toBeDefined();
            expect(error.originalError).toBeDefined();
          }
        );
      });
    });
  });

  describe('login() using Firebase', () => {
    it('should return user profile on successful login', () => {
      const credentials = AngularFireAuthMock.VALID_EMAIL_PASSWORD;
      spyOn(afAuth.auth, 'signInWithEmailAndPassword').and.callThrough();

      const response = authService.login(credentials);

      expect(afAuth.auth.signInWithEmailAndPassword).toHaveBeenCalledWith(credentials.email, credentials.password);

      response.subscribe(profile => {
        expect(profile).toEqual(AngularFireAuthMock.FAKE_PROFILE);
      });
    });

    it('should return options for an alert if login fails', () => {
      const credentials = AngularFireAuthMock.INVALID_EMAIL_PASSWORD;
      spyOn(afAuth.auth, 'signInWithEmailAndPassword').and.callThrough();

      const response = authService.login(credentials);

      expect(afAuth.auth.signInWithEmailAndPassword).toHaveBeenCalledWith(credentials.email, credentials.password);
      response.subscribe(
        _ => {},
        error => {
          expect(error.title).toBeDefined();
          expect(error.message).toBeDefined();
          expect(error.buttons).toBeDefined();
        }
      );
    });
  });

  describe('facebookAuth() using Facebook Native & Firebase', () => {

    it('should authenticate with firebase using facebook credentials obtained from native plugin', fakeAsync(() => {
      spyOn(facebook, 'login').and.callThrough();
      spyOn(afAuth.auth, 'signInWithCredential').and.callThrough();

      const result = authService.facebookAuth(FacebookMock.SUCCESSFUL_LOGIN);
      tick();

      expect(facebook.login).toHaveBeenCalled();
      expect(afAuth.auth.signInWithCredential).toHaveBeenCalled();
      expect(result instanceof Observable).toBeTruthy();

      result.subscribe(response => {
        expect(response).toEqual(AngularFireAuthMock.FAKE_PROFILE);
      });
    }));

    xit('should return options for an alert if facebook auth fails', fakeAsync(() => {
      spyOn(facebook, 'login').and.callThrough();

      const result = authService.facebookAuth(FacebookMock.FAILED_LOGIN);
      tick();

      expect(facebook.login).toHaveBeenCalled();
      expect(result instanceof Observable).toBeTruthy();

      result.subscribe(
        _ => {},
        error => {
          expect(error.title).toBeDefined();
          expect(error.message).toBeDefined();
          expect(error.buttons).toBeDefined();
        }
      );
    }));
  });

  describe('logout()', () => {
    it('should signout from Firebase on logout()', fakeAsync(() => {
      spyOn(afAuth.auth, 'signOut');

      authService.logout();
      tick();

      expect(afAuth.auth.signOut).toHaveBeenCalled();
    }));
  });

  describe('resetPassword() using Firebase', () => {
    it('should call the send password reset e-mail given valid input', () => {
      const credentials = AngularFireAuthMock.VALID_EMAIL_PASSWORD;

      spyOn(afAuth.auth, 'sendPasswordResetEmail').and.callThrough();
      spyOn(authService.toastCtrl, 'create').and.callThrough();

      const response = authService.resetPassword(credentials.email);
      response.subscribe(success => {
        expect(afAuth.auth.sendPasswordResetEmail).toHaveBeenCalledWith(credentials.email);
        expect(authService.toastCtrl.create).toHaveBeenCalled();
      });
    });

    it('should return options for an alert if login fails', () => {
      const credentials = AngularFireAuthMock.INVALID_EMAIL_PASSWORD;
      spyOn(afAuth.auth, 'sendPasswordResetEmail').and.callThrough();

      const response = authService.resetPassword(credentials.email);

      expect(afAuth.auth.sendPasswordResetEmail).toHaveBeenCalledWith(credentials.email);
      response.subscribe(
        _ => {},
        error => {
          expect(error.title).toBeDefined();
          expect(error.originalError).toBeDefined();
          expect(error.buttons).toBeDefined();
        }
      );
    });
  });

  describe('updateGeolocation()', () => {
    it('should set the geolocation for the current user on firebase', () => {
      const fakeUser = { uid: 'valid' };
      const fakeGeolocation = { latitude: 10, longitude: 10 };
      spyOn(afData, 'object').and.callFake((endpoint) => {
        expect(endpoint).toEqual(`profiles/${fakeUser.uid}/geolocation`);

        const mockFirebaseObjectObservable = { set() {} };

        spyOn(mockFirebaseObjectObservable, 'set')
          .and.callFake((geolocation) => {
            expect(geolocation).toEqual(fakeGeolocation);
          });

        return mockFirebaseObjectObservable;
      });

      (<any>_store).next({
        authentication: {
          user: fakeUser,
          geolocation: fakeGeolocation,
        }
      });

      authService.updateGeolocation();

      expect(afData.object).toHaveBeenCalled();
    });
  });

  describe('checkAuthState() using Firebase', () => {
    it('should observe an error when not authenticated', () => {
      afAuth.authState = Observable.of(null);
      spyOn(authService, 'checkAuthState').and.callThrough();

      authService.checkAuthState()
        .subscribe(
          _ => {},
          error => {
            expect(authService.checkAuthState).toHaveBeenCalled();
            expect(error).toBeTruthy();
          }
        );
    });

    it('should observe a user profile when authenticated', () => {
      afAuth.authState = <any>Observable.of(AngularFireAuthMock.FAKE_PROFILE);
      spyOn(authService, 'checkAuthState').and.callThrough();

      authService.checkAuthState()
        .subscribe(profile => {
          expect(authService.checkAuthState).toHaveBeenCalled();
          expect(profile).toEqual(AngularFireAuthMock.FAKE_PROFILE);
        });
    });
  });

});
