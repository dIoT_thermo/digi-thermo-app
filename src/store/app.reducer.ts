import { ActionReducer, combineReducers } from '@ngrx/store';
import { compose } from '@ngrx/core/compose';

import { storeFreeze } from 'ngrx-store-freeze';
import { createSelector } from 'reselect';

import { environment } from '../environment';
import * as fromNavigation from './navigation/navigation.reducer';
import * as fromAuthentication from './authentication/authentication.reducer';
import * as fromSensor from './sensor/sensor.reducer';
import * as fromProfile from './profile/profile.reducer';
import * as AuthActions from './authentication/authentication.actions';
import * as _ from 'lodash';

export interface State {
  navigation: fromNavigation.State;
  authentication: fromAuthentication.State;
  sensor: fromSensor.State;
  profile: fromProfile.State;
}

export const initialState: State = {
  navigation: fromNavigation.initialState,
  authentication: fromAuthentication.initialState,
  sensor: fromSensor.initialState,
  profile: fromProfile.initialState,
};

const reducers = {
  navigation: fromNavigation.reducer,
  authentication: fromAuthentication.reducer,
  sensor: fromSensor.reducer,
  profile: fromProfile.reducer
};

const developmentReducer: ActionReducer<State> = compose(storeFreeze, combineReducers)(reducers);
const productionReducer: ActionReducer<State> = combineReducers(reducers);

export function reducer(state = initialState, action: any) {
  if (action.type === AuthActions.LOGOUT) {
    return _.cloneDeep(initialState);
  } else if (environment.production) {
    return productionReducer(state, action);
  } else {
    return developmentReducer(state, action);
  }
}

// Navigation Selectors
export const getNavigationState = (state: State) => state.navigation;

// Authentication Selectors
export const getAuthenticationState = (state: State) => state.authentication;
export const getAuthIsRegistering = createSelector(getAuthenticationState, fromAuthentication.getIsRegistering);
export const getAuthIsLoading = createSelector(getAuthenticationState, fromAuthentication.getIsLoading);
export const getAuthIsAuthenticated = createSelector(getAuthenticationState, fromAuthentication.getIsAuthenticated);
export const getAuthIsResettingPassword = createSelector(getAuthenticationState, fromAuthentication.getIsResettingPassword);
export const getAuthUser = createSelector(getAuthenticationState, fromAuthentication.getUser);
export const getAuthError = createSelector(getAuthenticationState, fromAuthentication.getError);
export const getGeoLocation = createSelector(getAuthenticationState, fromAuthentication.getGeoLocation);

// Sensor Selectors
export const getSensorState = (state: State) => state.sensor;
export const getSensorIsLoading = createSelector(getSensorState, fromSensor.getIsLoading);
export const getSensorIsEnabled = createSelector(getSensorState, fromSensor.getIsEnabled);
export const getSensorDeviceList = createSelector(getSensorState, fromSensor.getDeviceList);
export const getSensorIsConnecting = createSelector(getSensorState, fromSensor.getIsConnecting);
export const getSensorIsConnected = createSelector(getSensorState, fromSensor.getIsConnected);
export const getSensorLastReading = createSelector(getSensorState, fromSensor.getLastReading);

// Profile Selectors
export const getProfileState = (state: State) => state.profile;
export const getProfileIsUpdating = createSelector(getProfileState, fromProfile.getIsUpdating);
export const getProfileTemperatureUnit = createSelector(getProfileState, fromProfile.getTemperatureUnit);
export const getProfileDataLogs = createSelector(getProfileState, fromProfile.getDataLogs);
export const getProfileError = createSelector(getProfileState, fromProfile.getError);
