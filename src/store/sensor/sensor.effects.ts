import { Injectable } from '@angular/core';

import { App } from 'ionic-angular';

import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

import { BluetoothService } from './../../providers/bluetooth.service';
import * as sensor from '../sensor/sensor.actions';
import * as navigation from '../navigation/navigation.actions';

@Injectable()
export class SensorEffects {
  @Effect()
  isBluetoothEnabled$: Observable<Action> = this.actions$
    .ofType(sensor.IS_BLUETOOTH_ENABLED)
    .switchMap((action: sensor.IsBluetoothEnabledAction) =>
      this.bluetoothService.isEnabled()
        .map(() => new sensor.IsBluetoothEnabledSuccessAction())
        .catch(() => Observable.of(new sensor.IsBluetoothEnabledFailedAction()))
    );

  @Effect()
  enabledSuccess$: Observable<Action> = this.actions$
    .ofType(sensor.IS_BLUETOOTH_ENABLED_SUCCESS)
    .map((action: sensor.IsBluetoothEnabledSuccessAction) => new sensor.ScanDevicesAction());

  @Effect({ dispatch: false })
  enabledFailed$: Observable<Action> = this.actions$
    .ofType(sensor.IS_BLUETOOTH_ENABLED_FAILED)
    .do(() => this.bluetoothService.forceBluetooth());


  @Effect({ dispatch: false })
  getConnections$: Observable<Action> = this.actions$
    .ofType(sensor.SCAN_DEVICES)
    .do(() => this.bluetoothService.scanDevices());

  @Effect({ dispatch: false })
  connect$: Observable<Action> = this.actions$
    .ofType(sensor.CONNECT)
    .do((action: sensor.ConnectAction) => this.bluetoothService.connectToDevice(action.payload));

  @Effect()
  isConnected$: Observable<Action> = this.actions$
    .ofType(sensor.IS_CONNECTED)
    .map((action: sensor.IsConnectedAction) =>
      new navigation.PushAction({
        name: 'TabsPage',
        options: { setRoot: true }
      })
    );

  @Effect({ dispatch: false })
  getSensorData$: Observable<Action> = this.actions$
    .ofType(sensor.GET_SENSOR_DATA)
    .do(() => this.bluetoothService.readDevice());

  @Effect({ dispatch: false })
  disconnect$: Observable<Action> = this.actions$
    .ofType(sensor.DISCONNECT)
    .do((action: sensor.DisconnectAction) => {
      this.bluetoothService.disconnectFromDevice(action.payload);
    });

  @Effect({ dispatch: false })
  setEmittingState$: Observable<Action> = this.actions$
    .ofType(sensor.SET_EMITTING_STATE)
    .do((action: sensor.SetEmittingStateAction) => {
      this.bluetoothService.setSensorEmittingState(action.payload);
    });

  constructor(
    private actions$: Actions,
    private bluetoothService: BluetoothService
  ) {}

}
