import * as actions from './sensor.actions';

describe('Sensor Actions', () => {
  let payload;
  let action: actions.Actions;

  it('should create IsBluetoothEnabledAction', () => {
    action = new actions.IsBluetoothEnabledAction();

    expect(action instanceof actions.IsBluetoothEnabledAction).toBeTruthy();
    expect(action.type).toEqual(actions.IS_BLUETOOTH_ENABLED);
    expect(action.payload).toBeUndefined();
  });

  it('should create IsBluetoothEnabledSuccessAction', () => {
    action = new actions.IsBluetoothEnabledSuccessAction();

    expect(action instanceof actions.IsBluetoothEnabledSuccessAction).toBeTruthy();
    expect(action.type).toEqual(actions.IS_BLUETOOTH_ENABLED_SUCCESS);
    expect(action.payload).toBeUndefined();
  });

  it('should create IsBluetoothEnabledFailedAction', () => {
    action = new actions.IsBluetoothEnabledFailedAction();

    expect(action instanceof actions.IsBluetoothEnabledFailedAction).toBeTruthy();
    expect(action.type).toEqual(actions.IS_BLUETOOTH_ENABLED_FAILED);
    expect(action.payload).toBeUndefined();
  });

  it('should create ScanDevicesAction', () => {
    action = new actions.ScanDevicesAction();

    expect(action instanceof actions.ScanDevicesAction).toBeTruthy();
    expect(action.type).toEqual(actions.SCAN_DEVICES);
    expect(action.payload).toBeUndefined();
  });

  it('should create DeviceFoundAction', () => {
    payload = {
      name: 'Digi Thermo Device',
      id: '193fi045n499fn3o4398943jg42'
    };

    action = new actions.DeviceFoundAction(payload);

    expect(action instanceof actions.DeviceFoundAction).toBeTruthy();
    expect(action.type).toEqual(actions.DEVICE_FOUND);
    expect(action.payload).toEqual(payload);
  });

  it('should create ConnectAction', () => {
    const name = 'Fake Device Name';
    const id = 1;
    payload = { name, id };
    action = new actions.ConnectAction(payload);

    expect(action instanceof actions.ConnectAction).toBeTruthy();
    expect(action.type).toEqual(actions.CONNECT);
    expect(action.payload).toEqual(payload);
  });

  it('should create IsConnectedAction', () => {
    payload = {
      id: '193fi045n499fn3o4398943jg42',
      name: 'Digi Thermo Device',
      characteristics: [],
      services: []
    };
    action = new actions.IsConnectedAction(payload);

    expect(action instanceof actions.IsConnectedAction).toBeTruthy();
    expect(action.type).toEqual(actions.IS_CONNECTED);
    expect(action.payload).toEqual(payload);
  });

  it('should create CheckConnectionAction', () => {
    action = new actions.CheckConnectionAction();

    expect(action instanceof actions.CheckConnectionAction).toBeTruthy();
    expect(action.type).toEqual(actions.CHECK_CONNECTION);
  });

  it('should create GetSensorDataAction', () => {
    action = new actions.GetSensorDataAction();

    expect(action instanceof actions.GetSensorDataAction).toBeTruthy();
    expect(action.type).toEqual(actions.GET_SENSOR_DATA);
  });

  it('should create GetSensorDataSuccessAction', () => {
    const temperature = 10;
    const humidity = 10;
    payload = { temperature, humidity };
    action = new actions.GetSensorDataSuccessAction(payload);

    expect(action instanceof actions.GetSensorDataSuccessAction).toBeTruthy();
    expect(action.type).toEqual(actions.GET_SENSOR_DATA_SUCCESS);
    expect(action.payload).toEqual(payload);
  });

  it('should create SetEmittingStateAction', () => {
    payload = 1;
    action = new actions.SetEmittingStateAction(payload);

    expect(action instanceof actions.SetEmittingStateAction).toBeTruthy();
    expect(action.type).toEqual(actions.SET_EMITTING_STATE);
    expect(action.payload).toEqual(payload);
  });

  it('should create DisconnectAction', () => {
    action = new actions.DisconnectAction();

    expect(action instanceof actions.DisconnectAction).toBeTruthy();
    expect(action.type).toEqual(actions.DISCONNECT);
  });
});
