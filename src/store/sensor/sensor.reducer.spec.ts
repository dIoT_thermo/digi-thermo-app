import * as sensor from './sensor.types';
import * as actions from './sensor.actions';
import { reducer } from './sensor.reducer';

describe('Sensor Reducer', () => {
    let payload;
    let action: actions.Actions;
    let expectedState: sensor.State;
    let newState;

    const initialState = sensor.initialState;

    beforeEach(() => {
        expectedState = JSON.parse(JSON.stringify(initialState));
    });

    it('should return initial state for unhandled actions', () => {
        action = <any>'fakeAction';

        newState = reducer(undefined, action);

        expect(newState).toEqual(expectedState);
    });

    it('should handle IS_BLUETOOTH_ENABLED', () => {
        action = new actions.IsBluetoothEnabledAction();
        expectedState.isLoading = true;

        newState = reducer(undefined, action);

        expect(newState).toEqual(expectedState);
    });

    it('should handle IS_BLUETOOTH_ENABLED_SUCCESS', () => {
        action = new actions.IsBluetoothEnabledSuccessAction();
        expectedState.isLoading = false,
        expectedState.isEnabled = true,

        newState = reducer(undefined, action);

        expect(newState).toEqual(expectedState);
    });

    it('should handle IS_BLUETOOTH_ENABLED_FAILED', () => {
        action = new actions.IsBluetoothEnabledFailedAction();
        expectedState.isLoading = false,
        expectedState.isEnabled = false,

        newState = reducer(undefined, action);

        expect(newState).toEqual(expectedState);
    });

    it('should handle SCAN_DEVICES', () => {
        action = new actions.ScanDevicesAction();
        expectedState.isConnecting = false;
        expectedState.deviceList = [];

        newState = reducer(undefined, action);

        expect(newState).toEqual(expectedState);
    });

    it('should handle DEVICE_FOUND', () => {
        payload = { name: 'sensor', id: '92u394839f9982939r83928r32' };
        action = new actions.DeviceFoundAction(payload);
        expectedState.isLoading = false;
        expectedState.isEnabled = true;
        expectedState.deviceList = [payload];

        newState = reducer(undefined, action);

        expect(newState).toEqual(expectedState);
    });

    it('should handle CONNECT', () => {
        action = new actions.ConnectAction(payload);
        expectedState.isConnecting = true;
        expectedState.isConnected = false;

        newState = reducer(undefined, action);

        expect(newState).toEqual(expectedState);
    });

    it('should handle IS_CONNECTED', () => {
        action = new actions.IsConnectedAction(payload);
        expectedState.isLoading = false;
        expectedState.isEnabled = true;
        expectedState.isConnecting = false;
        expectedState.isConnected = true;

        newState = reducer(undefined, action);

        expect(newState).toEqual(expectedState);
    });

    it('should handle GET_SENSOR_DATA_SUCCESS', () => {
      payload = { temperature: 10, humidity: 10 };
      action = new actions.GetSensorDataSuccessAction(payload);
      expectedState.lastReading = payload;

      newState = reducer(undefined, action);

      expect(newState).toEqual(expectedState);
    });
});
