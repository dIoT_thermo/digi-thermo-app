import { Action } from '@ngrx/store';
import { State, IDevice, IConnectedDevice, IDeviceReading } from './sensor.types';

import { IErrorAlert } from './../../models/error-alert';

export const IS_BLUETOOTH_ENABLED = '[Sensor] Is Bluetooth Enabled';
export class IsBluetoothEnabledAction implements Action {
  readonly type = IS_BLUETOOTH_ENABLED;
  constructor(public payload?: any) {}
}

export const IS_BLUETOOTH_ENABLED_SUCCESS = '[Sensor] Is Bluetooth Enabled Success';
export class IsBluetoothEnabledSuccessAction implements Action {
  readonly type = IS_BLUETOOTH_ENABLED_SUCCESS;
  constructor(public payload?: any) {}
}

export const IS_BLUETOOTH_ENABLED_FAILED = '[Sensor] Is Bluetooth Enabled Failed';
export class IsBluetoothEnabledFailedAction implements Action {
  readonly type = IS_BLUETOOTH_ENABLED_FAILED;
  constructor(public payload?: any) {}
}

export const SCAN_DEVICES = '[Sensor] Scan Devices';
export class ScanDevicesAction implements Action {
  readonly type = SCAN_DEVICES;
  constructor(public payload?: any) {}
}

export const DEVICE_FOUND = '[Sensor] Device Found';
export class DeviceFoundAction implements Action {
  readonly type = DEVICE_FOUND;
  constructor(public payload: IDevice) {}
}

export const CONNECT = '[Sensor] Connect';
export class ConnectAction implements Action {
  readonly type = CONNECT;
  constructor(public payload: IDevice) {}
}

export const IS_CONNECTED = '[Sensor] Is Connected';
export class IsConnectedAction implements Action {
  readonly type = IS_CONNECTED;
  constructor(public payload?: any) {}
}

export const CHECK_CONNECTION = '[Sensor] Check Connection';
export class CheckConnectionAction implements Action {
  readonly type = CHECK_CONNECTION;
  constructor(public payload?: any) {}
}

export const GET_SENSOR_DATA = '[Sensor] Get Sensor Data';
export class GetSensorDataAction implements Action {
  readonly type = GET_SENSOR_DATA;
  constructor(public payload?: any) {}
}

export const GET_SENSOR_DATA_SUCCESS = '[Sensor] Get Sensor Data Success';
export class GetSensorDataSuccessAction implements Action {
  readonly type = GET_SENSOR_DATA_SUCCESS;
  constructor(public payload: IDeviceReading) {}
}

export const SET_EMITTING_STATE = '[Sensor] Set Emitting State';
export class SetEmittingStateAction implements Action {
  readonly type = SET_EMITTING_STATE;
  constructor(public payload: number) { }
}

export const DISCONNECT = '[Sensor] Disconnect Sensor';
export class DisconnectAction implements Action {
  readonly type = DISCONNECT;
  constructor(public payload?: any) { }
}

export type Actions
  = IsBluetoothEnabledAction
  | IsBluetoothEnabledSuccessAction
  | IsBluetoothEnabledFailedAction
  | ScanDevicesAction
  | DeviceFoundAction
  | ConnectAction
  | IsConnectedAction
  | CheckConnectionAction
  | GetSensorDataAction
  | GetSensorDataSuccessAction
  | SetEmittingStateAction
  | DisconnectAction;
