import * as sensor from './sensor.actions';
import { State, initialState, IDevice, IConnectedDevice } from './sensor.types';
import { IErrorAlert } from './../../models/error-alert';
import * as _ from 'lodash';

export { State, initialState };

export function reducer(state = initialState, action: sensor.Actions): State {
  const type = action.type;

  switch (type) {
    case sensor.IS_BLUETOOTH_ENABLED: {
      return {
        ...state,
        isLoading: true,
      };
    }

    case sensor.IS_BLUETOOTH_ENABLED_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        isEnabled: true,
      };
    }

    case sensor.IS_BLUETOOTH_ENABLED_FAILED: {
      return {
        ...state,
        isLoading: false,
        isEnabled: false,
      };
    }

    case sensor.SCAN_DEVICES: {
      return {
        ...state,
        isConnecting: false,
        deviceList: [],
      };
    }

    case sensor.DEVICE_FOUND: {
      return {
        ...state,
        isLoading: false,
        isEnabled: true,
        deviceList: _.uniqBy([...state.deviceList, action.payload], 'id'),
      };
    }

    case sensor.CONNECT: {
      return {
        ...state,
        isConnecting: true,
        isConnected: false,
      };
    }

    case sensor.IS_CONNECTED: {
      return {
        ...state,
        isLoading: false,
        isEnabled: true,
        isConnecting: false,
        isConnected: true
      };
    }

    case sensor.GET_SENSOR_DATA_SUCCESS: {
      return {
        ...state,
        lastReading: action.payload
      };
    }

    default: {
      return state;
    }
  }
}

export const getIsLoading = (state: State) => state.isLoading;
export const getIsEnabled = (state: State) => state.isEnabled;
export const getDeviceList = (state: State) => state.deviceList;
export const getIsConnecting = (state: State) => state.isConnecting;
export const getIsConnected = (state: State) => state.isConnected;
export const getLastReading = (state: State) => state.lastReading;
