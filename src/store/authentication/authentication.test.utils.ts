import * as authentication from './authentication.types';

export function getFakeBasicProfile(): authentication.IFirebaseBasicProfile {
  return {
    displayName: 'FakeUser',
    email: 'fake@email.com',
    photoURL: 'http://fakephoto.com/123',
    providerId: 'fakeProvider',
    uid: '1',
  };
}

export function getFakeUserProfile(): authentication.IFirebaseUserProfile {
  return {
    ...getFakeBasicProfile(),
    emailVerified: true,
    isAnonymous: false,
    providerData: null,
    refreshToken: 'fakeToken'
  };
}

export function getFakeGeolocation(): authentication.IGeolocation {
  return {
    latitude: 6534,
    longitude: 3945
  };
}
