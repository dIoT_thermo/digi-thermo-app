import { Action } from '@ngrx/store';

import { ILoginCredentials, IFirebaseUserProfile, IGeolocation } from './authentication.types';
import { IErrorAlert } from './../../models/error-alert';

export const LOGIN = '[Authentication] Login';
export class LoginAction implements Action {
  readonly type = LOGIN;
  constructor(public payload: ILoginCredentials) {}
}

export const LOGIN_SUCCESS = '[Authentication] Login Success';
export class LoginSuccessAction implements Action {
  readonly type = LOGIN_SUCCESS;
  constructor(public payload: IFirebaseUserProfile) {}
}

export const LOGIN_FAILED = '[Authentication] Login Failed';
export class LoginFailedAction implements Action {
  readonly type = LOGIN_FAILED;
  constructor(public payload: IErrorAlert) {}
}

export const FACEBOOK_LOGIN = '[Authentication] Facebook Login';
export class FacebookLoginAction implements Action {
  readonly type = FACEBOOK_LOGIN;
  constructor(public payload?: any) {}
}

export const FACEBOOK_LOGIN_SUCCESS = '[Authentication] Facebook Login Success';
export class FacebookLoginSuccessAction implements Action {
  readonly type = FACEBOOK_LOGIN_SUCCESS;
  constructor(public payload: IFirebaseUserProfile) {}
}

export const FACEBOOK_LOGIN_FAILED = '[Authentication] Facebook Login Failed';
export class FacebookLoginFailedAction implements Action {
  readonly type = FACEBOOK_LOGIN_FAILED;
  constructor(public payload: IErrorAlert) {}
}

export const LOGOUT = '[Authentication] Logout';
export class LogoutAction implements Action {
  readonly type = LOGOUT;
  constructor(public payload?: any) {}
}

export const LOGOUT_SUCCESS = '[Authentication] Logout Success';
export class LogoutSuccessAction implements Action {
  readonly type = LOGOUT_SUCCESS;
  constructor(public payload?: any) {}
}

export const LOGOUT_FAILED = '[Authentication] Logout Failed';
export class LogoutFailedAction implements Action {
  readonly type = LOGOUT_FAILED;
  constructor(public payload: IErrorAlert) {}
}

export const RESET_PASSWORD = '[Authentication] Reset Password';
export class ResetPasswordAction implements Action {
  readonly type = RESET_PASSWORD;
  constructor(public payload: string) {}
}

export const RESET_PASSWORD_SUCCESS = '[Authentication] Reset Password Success';
export class ResetPasswordSuccessAction implements Action {
  readonly type = RESET_PASSWORD_SUCCESS;
  constructor(public payload?: any) {}
}

export const RESET_PASSWORD_FAILED = '[Authentication] Reset Password Failed';
export class ResetPasswordFailedAction implements Action {
  readonly type = RESET_PASSWORD_FAILED;
  constructor(public payload: IErrorAlert) {}
}

export const REGISTER = '[Authentication] Register';
export class RegisterAction implements Action {
  readonly type = REGISTER;
  constructor(public payload: ILoginCredentials) {}
}

export const REGISTER_FAILED = '[Authentication] Register Failed';
export class RegisterFailedAction implements Action {
  readonly type = REGISTER_FAILED;
  constructor(public payload: IErrorAlert) {}
}

export const SET_GEOLOCATION = '[Geolocation] Set Geolocation';
export class SetGeolocationAction implements Action {
    readonly type = SET_GEOLOCATION;
    constructor(public payload: IGeolocation) {}
}

export const DISMISS_ERROR = '[Authentication] Dismiss Error';
export class DismissErrorAction implements Action {
  readonly type = DISMISS_ERROR;
  constructor(public payload?: any) {}
}

export const CHECK_AUTH_STATE = '[Authentication] Check Authentication State';
export class CheckAuthStateAction implements Action {
  readonly type = CHECK_AUTH_STATE;
  constructor(public payload?: any) {}
}

export type Actions
  = LoginAction
  | LoginSuccessAction
  | LoginFailedAction
  | FacebookLoginAction
  | FacebookLoginSuccessAction
  | FacebookLoginFailedAction
  | LogoutAction
  | LogoutSuccessAction
  | LogoutFailedAction
  | ResetPasswordAction
  | ResetPasswordSuccessAction
  | ResetPasswordFailedAction
  | RegisterAction
  | RegisterFailedAction
  | DismissErrorAction
  | CheckAuthStateAction
  | SetGeolocationAction;
