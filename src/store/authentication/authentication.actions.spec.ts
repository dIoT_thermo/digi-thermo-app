import * as actions from './authentication.actions';
import { getFakeBasicProfile, getFakeUserProfile, getFakeGeolocation } from './authentication.test.utils';

describe('Authentication Actions', () => {
  let payload;
  let action: actions.Actions;

  it('should create LoginAction', () => {
    const email = 'fake@email.com';
    const password = '123test';
    payload = { email, password };

    action = new actions.LoginAction(payload);

    expect(action instanceof actions.LoginAction).toBeTruthy();
    expect(action.type).toEqual(actions.LOGIN);
    expect(action.payload).toEqual(payload);
  });

  it('should create LoginSuccessAction', () => {
    payload = getFakeUserProfile();

    action = new actions.LoginSuccessAction(payload);

    expect(action instanceof actions.LoginSuccessAction).toBeTruthy();
    expect(action.type).toEqual(actions.LOGIN_SUCCESS);
    expect(action.payload).toEqual(payload);
  });

  it('should create LoginFailedAction', () => {
    const message = 'Fake Error';
    payload = { message };

    action = new actions.LoginFailedAction(payload);

    expect(action instanceof actions.LoginFailedAction).toBeTruthy();
    expect(action.type).toEqual(actions.LOGIN_FAILED);
    expect(action.payload).toEqual(payload);
  });

  it('should create FacebookLoginAction', () => {
    action = new actions.FacebookLoginAction();

    expect(action instanceof actions.FacebookLoginAction).toBeTruthy();
    expect(action.type).toEqual(actions.FACEBOOK_LOGIN);
    expect(action.payload).toBeUndefined();
  });

  it('should create FacebookLoginSuccessAction', () => {
    payload = getFakeUserProfile();
    payload.providerData = getFakeBasicProfile();

    action = new actions.FacebookLoginSuccessAction(payload);

    expect(action instanceof actions.FacebookLoginSuccessAction).toBeTruthy();
    expect(action.type).toEqual(actions.FACEBOOK_LOGIN_SUCCESS);
    expect(action.payload).toEqual(payload);
  });

  it('should create FacebookLoginFailedAction', () => {
    const message = 'Fake Error';
    payload = { message };

    action = new actions.FacebookLoginFailedAction(payload);

    expect(action instanceof actions.FacebookLoginFailedAction).toBeTruthy();
    expect(action.type).toEqual(actions.FACEBOOK_LOGIN_FAILED);
    expect(action.payload).toEqual(payload);
  });

  it('should create RegisterAction', () => {
    const email = 'fake@email.com';
    const password = '123test';
    payload = { email, password };

    action = new actions.RegisterAction(payload);

    expect(action instanceof actions.RegisterAction).toBeTruthy();
    expect(action.type).toEqual(actions.REGISTER);
    expect(action.payload).toEqual(payload);
  });

  it('should create RegisterFailedAction', () => {
    const message = 'Fake Error';
    payload = { message };

    action = new actions.RegisterFailedAction(payload);

    expect(action instanceof actions.RegisterFailedAction).toBeTruthy();
    expect(action.type).toEqual(actions.REGISTER_FAILED);
    expect(action.payload).toEqual(payload);
  });

  it('should create a DismissErrorAction', () => {
    action = new actions.DismissErrorAction();

    expect(action instanceof actions.DismissErrorAction).toBeTruthy();
    expect(action.type).toEqual(actions.DISMISS_ERROR);
    expect(action.payload).toBeUndefined();
  });

  it('should create LogoutAction', () => {
    action = new actions.LogoutAction();

    expect(action instanceof actions.LogoutAction).toBeTruthy();
    expect(action.type).toEqual(actions.LOGOUT);
    expect(action.payload).toBeUndefined();
  });

  it('should create LogoutSuccessAction', () => {
    action = new actions.LogoutSuccessAction();

    expect(action instanceof actions.LogoutSuccessAction).toBeTruthy();
    expect(action.type).toEqual(actions.LOGOUT_SUCCESS);
    expect(action.payload).toBeUndefined();
  });

  it('should create LogoutFailedAction', () => {
    const message = 'Fake Error';
    payload = { message };

    action = new actions.LogoutFailedAction(payload);

    expect(action instanceof actions.LogoutFailedAction).toBeTruthy();
    expect(action.type).toEqual(actions.LOGOUT_FAILED);
    expect(action.payload).toEqual(payload);
  });

  it('should create ResetPasswordAction', () => {
    const email = 'FakeOut@gmail.com';

    action = new actions.ResetPasswordAction(email);

    expect(action instanceof actions.ResetPasswordAction).toBeTruthy();
    expect(action.type).toEqual(actions.RESET_PASSWORD);
    expect(action.payload).toEqual(email);
  });

  it('should create ResetPasswordSuccessAction', () => {

    action = new actions.ResetPasswordSuccessAction();

    expect(action instanceof actions.ResetPasswordSuccessAction).toBeTruthy();
    expect(action.type).toEqual(actions.RESET_PASSWORD_SUCCESS);
    expect(action.payload).toBeUndefined();
  });

  it('should create ResetPasswordFailedAction', () => {
    const message = 'Fake Error';
    payload = { message };

    action = new actions.ResetPasswordFailedAction(payload);

    expect(action instanceof actions.ResetPasswordFailedAction).toBeTruthy();
    expect(action.type).toEqual(actions.RESET_PASSWORD_FAILED);
    expect(action.payload).toEqual(payload);
  });

  it('should create CheckAuthStateAction', () => {
    action = new actions.CheckAuthStateAction();

    expect(action instanceof actions.CheckAuthStateAction).toBeTruthy();
    expect(action.type).toEqual(actions.CHECK_AUTH_STATE);
    expect(action.payload).toBeUndefined();
  });

  it('should create SetGeolocationAction', () => {
    payload = { latitude: 10, longitude: 10 };
    action = new actions.SetGeolocationAction(payload);

    expect(action instanceof actions.SetGeolocationAction).toBeTruthy();
    expect(action.type).toEqual(actions.SET_GEOLOCATION);
    expect(action.payload).toEqual(payload);
  });
});
