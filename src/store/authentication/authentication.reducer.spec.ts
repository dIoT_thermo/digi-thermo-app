import * as authentication from './authentication.types';
import * as actions from './authentication.actions';
import { reducer } from './authentication.reducer';
import { getFakeBasicProfile, getFakeUserProfile, getFakeGeolocation } from './authentication.test.utils';

describe('Authentication Reducer', () => {
  let payload;
  let action: actions.Actions;
  let expectedState: authentication.State;
  let newState;

  const initialState = authentication.initialState;

  beforeEach(() => {
    expectedState = JSON.parse(JSON.stringify(initialState));
  });

  it('should return initial state for unhandled actions', () => {
    action = <any>'fakeAction';

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  describe('Login Actions', () => {
    it('should handle LOGIN action', () => {
      const email = 'fake@email.com';
      const password = '123test';
      payload = { email, password };
      action = new actions.LoginAction(payload);
      expectedState.isLoading = true;

      newState = reducer(undefined, action);

      expect(newState).toEqual(expectedState);
    });

    it('should handle FACEBOOK_LOGIN', () => {
      action = new actions.FacebookLoginAction();
      expectedState.isLoading = true;

      newState = reducer(undefined, action);

      expect(newState).toEqual(expectedState);
    });
  });

  describe('Successful Logins', () => {
    it('should handle LOGIN_SUCCESS action', () => {
      payload = getFakeUserProfile();
      action = new actions.LoginSuccessAction(payload);
      expectedState.isAuthenticated = true;
      expectedState.user = payload;
      expectedState.error = null;

      newState = reducer(undefined, action);

      expect(newState).toEqual(expectedState);
    });

    it('should handle FACEBOOK_LOGIN_SUCCESS', () => {
      payload = getFakeUserProfile();
      action = new actions.FacebookLoginSuccessAction(payload);
      expectedState.isAuthenticated = true;
      expectedState.user = payload;
      expectedState.error = null;

      newState = reducer(undefined, action);

      expect(newState).toEqual(expectedState);
    });
  });

  describe('Failed Logins', () => {
    it('should handle LOGIN_FAILED action', () => {
      const message = 'Fake Error';
      payload = { message };
      action = new actions.LoginFailedAction(payload);
      expectedState.error = payload;

      newState = reducer(undefined, action);

      expect(newState).toEqual(expectedState);
    });

    it('should handle FACEBOOK_LOGIN_FAILED action', () => {
      const message = 'Fake Error';
      payload = { message };
      action = new actions.FacebookLoginFailedAction(payload);
      expectedState.error = payload;

      newState = reducer(undefined, action);

      expect(newState).toEqual(expectedState);
    });
  });

  describe('Register Actions', () => {
    it('should handle REGISTER action', () => {
      const email = 'fake@email.com';
      const password = '123test';
      payload = { email, password };
      action = new actions.RegisterAction(payload);
      expectedState.isRegistering = true;

      newState = reducer(undefined, action);

      expect(newState).toEqual(expectedState);
    });
  });

  describe('Failed Registeration', () => {
    it('should handle REGISTER_FAILED action', () => {
      const message = 'Fake Error';
      payload = { message };
      action = new actions.RegisterFailedAction(payload);
      expectedState.error = payload;

      newState = reducer(undefined, action);

      expect(newState).toEqual(expectedState);
    });

    it('should handle DISMISS_ERROR action', () => {
      action = new actions.DismissErrorAction();
      expectedState.error = null;

      newState = reducer(undefined, action);

      expect(newState).toEqual(expectedState);
    });
  });

  it('should handle LOGOUT action', () => {
    action = new actions.LogoutAction();
    expectedState.isLoading = true;

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle LOGOUT_SUCCESS action', () => {
    action = new actions.LogoutSuccessAction();

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle LOGOUT_FAILED action', () => {
    const message = 'Fake Error';
    payload = { message };
    action = new actions.LogoutFailedAction(payload);
    expectedState.error = payload;

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle SET_GEOLOCATION action', () => {
    const latitude = 10;
    const longitude = 10;
    payload = { latitude, longitude };
    action = new actions.SetGeolocationAction(payload);
    expectedState.geolocation = payload;

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  describe('Reset Password Actions', () => {
    it('should handle RESET_PASSWORD action', () => {
      const email = 'fake@email.com';
      action = new actions.ResetPasswordAction(email);
      expectedState.isResettingPassword = true;

      newState = reducer(undefined, action);

      expect(newState).toEqual(expectedState);
    });

    it('should handle RESET_PASSWORD_SUCCESS action', () => {
      action = new actions.ResetPasswordSuccessAction();

      newState = reducer(undefined, action);

      expect(newState).toEqual(expectedState);
    });

    it('should handle RESET_PASSWORD_FAILED action', () => {
      const message = 'Fake Error';
      payload = { message };
      action = new actions.ResetPasswordFailedAction(payload);
      expectedState.error = payload;

      newState = reducer(undefined, action);

      expect(newState).toEqual(expectedState);
    });

    describe('Failed Logins', () => {
      it('should handle LOGIN_FAILED action', () => {
        const message = 'Fake Error';
        payload = { message };
        action = new actions.LoginFailedAction(payload);
        expectedState.error = payload;

        newState = reducer(undefined, action);

        expect(newState).toEqual(expectedState);
      });

      it('should handle FACEBOOK_LOGIN_FAILED action', () => {
        const message = 'Fake Error';
        payload = { message };
        action = new actions.FacebookLoginFailedAction(payload);
        expectedState.error = payload;

        newState = reducer(undefined, action);

        expect(newState).toEqual(expectedState);
      });
    });

  });
});
