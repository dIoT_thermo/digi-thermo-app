import { Injectable } from '@angular/core';

import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/merge';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/empty';

import { IFirebaseUserProfile, ILoginCredentials, IGeolocation } from './authentication.types';
import { AuthService } from './../../providers/auth.service';
import * as authentication from './authentication.actions';
import * as navigation from '../navigation/navigation.actions';
import * as sensor from '../sensor/sensor.actions';

@Injectable()
export class AuthenticationEffects {
  @Effect()
  login$: Observable<Action> = this.actions$
    .ofType(authentication.LOGIN)
    .switchMap((action: authentication.LoginAction) =>
      this.authService.login(action.payload)
        .map((user: IFirebaseUserProfile) => new authentication.LoginSuccessAction(user))
        .catch(error => Observable.of(new authentication.LoginFailedAction(error)))
    );

  @Effect()
  resetPassword$: Observable<Action> = this.actions$
    .ofType(authentication.RESET_PASSWORD)
    .switchMap((action: authentication.ResetPasswordAction) =>
      this.authService.resetPassword(action.payload)
        .map(() => new authentication.ResetPasswordSuccessAction())
        .catch(error => Observable.of(new authentication.ResetPasswordFailedAction(error)))
    );

  @Effect()
  resetPasswordSuccess$: Observable<Action> = this.actions$
    .ofType(authentication.RESET_PASSWORD_SUCCESS)
    .switchMap((action: authentication.ResetPasswordSuccessAction) =>
      Observable.of(new navigation.PopAction())
    );

  @Effect()
  facebookLogin$: Observable<Action> = this.actions$
    .ofType(authentication.FACEBOOK_LOGIN)
    .switchMap((action: authentication.FacebookLoginAction) =>
      this.authService.facebookAuth(['email'])
        .map((user: IFirebaseUserProfile) => new authentication.FacebookLoginSuccessAction(user))
        .catch(error => Observable.of(new authentication.FacebookLoginFailedAction(error)))
    );

  @Effect()
  loginSuccess$: Observable<Action> = this.actions$
    .ofType(authentication.LOGIN_SUCCESS, authentication.FACEBOOK_LOGIN_SUCCESS)
    .map((action: authentication.LoginSuccessAction | authentication.FacebookLoginSuccessAction) =>
      new navigation.PushAction({
        name: 'ConnectSensorPage',
        options: { setRoot: true }
      })
    )
    .do(() => this.authService.updateGeolocation());

  @Effect()
  logout$: Observable<Action> = this.actions$
    .ofType(authentication.LOGOUT)
    .switchMap((action: authentication.LogoutAction) =>
      this.authService.logout()
        .map(() => new navigation.PushAction({ name: 'LoginPage', options: { setRoot: true } }))
        .catch(error => Observable.of(new authentication.LogoutFailedAction(error)))
    );

  @Effect()
  register$: Observable<Action> = this.actions$
    .ofType(authentication.REGISTER)
    .switchMap((action: authentication.RegisterAction) =>
      this.authService.signUp(action.payload)
      .map((credentials: ILoginCredentials) => new authentication.LoginAction(credentials))
      .catch(error => Observable.of(new authentication.RegisterFailedAction(error)))
    );

  @Effect()
  checkAuthState$: Observable<Action> = this.actions$
    .ofType(authentication.CHECK_AUTH_STATE)
    .switchMap((action: authentication.CheckAuthStateAction) =>
      this.authService.checkAuthState()
        .map((user: IFirebaseUserProfile) => new authentication.LoginSuccessAction(user))
        .catch(error => Observable.empty())
    );

  constructor(
    private actions$: Actions,
    private authService: AuthService,
  ) { }
}
