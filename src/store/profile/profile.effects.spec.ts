import { TestBed } from '@angular/core/testing';

import { App, NavController } from 'ionic-angular';

import { EffectsTestingModule, EffectsRunner } from '@ngrx/effects/testing';

import { Observable } from 'rxjs/Observable';

import * as actions from './profile.actions';
import { ProfileEffects } from './profile.effects';
import { ProfileService } from './../../providers/profile.service';
import { ProfileServiceMock } from './../../mocks/profile.service.mock';
import { AppMock } from './../../mocks/ionic/app.mock';

describe('ProfileEffects', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      EffectsTestingModule
    ],
    providers: [
      ProfileEffects,
      {
        provide: ProfileService,
        useClass: ProfileServiceMock,
      },
      {
        provide: App,
        useClass: AppMock,
      }
    ]
  }));

  let runner: EffectsRunner;
  let profileEffects: ProfileEffects;
  let profileService: ProfileService;

  beforeEach(() => {
    runner = TestBed.get(EffectsRunner);
    profileEffects = TestBed.get(ProfileEffects);
    profileService = TestBed.get(ProfileService);
  });

  it('should return GET_DATA_LOG_SUCCESS on GET_DATA_LOG success', () => {
    spyOn(profileService, 'getDataLogs').and.callThrough();

    runner.queue(new actions.GetDataLogsAction());
    profileEffects.getDataLogs$
      .subscribe(result => {
        expect(result.type).toBe(actions.GET_DATA_LOGS_SUCCESS);
      });

    expect(profileService.getDataLogs).toHaveBeenCalledTimes(1);
  });

  it('should return GET_DATA_LOG_FAILED on GET_DATA_LOG error', () => {
    spyOn(profileService, 'getDataLogs').and.returnValue(Observable.throw({}));

    runner.queue(new actions.GetDataLogsAction());
    profileEffects.getDataLogs$
      .subscribe(result => {
        expect(result.type).toBe(actions.GET_DATA_LOGS_FAILED);
      });

    expect(profileService.getDataLogs).toHaveBeenCalledTimes(1);
  });

  it('should return UPDATE_DATA_LOG_SUCCESS on UPDATE_DATA_LOG success', () => {
    const $key = 'fakeKey';
    const updates = { title: 'fake' };
    const payload = { $key, updates };
    spyOn(profileService, 'updateDataLog').and.callThrough();

    runner.queue(new actions.UpdateDataLogAction(payload));
    profileEffects.updateDataLogs$
      .subscribe(result => {
        expect(result.type).toBe(actions.UPDATE_DATA_LOG_SUCCESS);
      });

    expect(profileService.updateDataLog).toHaveBeenCalledWith(payload);
  });

  it('should return UPDATE_DATA_LOG_FAILED on UPDATE_DATA_LOG error', () => {
    const $key = 'fakeKey';
    const updates = { title: 'fake' };
    const payload = { $key, updates };
    spyOn(profileService, 'updateDataLog').and.returnValue(Observable.throw({}));

    runner.queue(new actions.UpdateDataLogAction(payload));
    profileEffects.updateDataLogs$
      .subscribe(result => {
        expect(result.type).toBe(actions.UPDATE_DATA_LOG_FAILED);
      });

    expect(profileService.updateDataLog).toHaveBeenCalledWith(payload);
  });

  it('should return UPDATE_TEMPERATURE_UNIT_SUCCESS action on UPDATE_TEMPERATURE_UNIT success', () => {
    const payload = 'F';
    spyOn(profileService, 'updateUnit').and.callThrough();

    runner.queue(new actions.UpdateTemperatureUnitAction(payload));
    profileEffects.updateTemperatureUnit$
      .subscribe(result => {
        expect(result.type).toBe(actions.UPDATE_TEMPERATURE_UNIT_SUCCESS);
      });

    expect(profileService.updateUnit).toHaveBeenCalledWith(payload);
  });

  it('should return UPDATE_TEMPERATURE_UNIT_FAILED action on UPDATE_TEMPERATURE_UNIT error', () => {
    const payload = 'Please check that your device is connected to the internet';
    spyOn(profileService, 'updateUnit')
      .and.returnValue(Observable.throw({}));

    runner.queue(new actions.UpdateTemperatureUnitAction(payload));
    profileEffects.updateTemperatureUnit$
      .subscribe(result => {
        expect(result.type).toBe(actions.UPDATE_TEMPERATURE_UNIT_FAILED);
      });

    expect(profileService.updateUnit).toHaveBeenCalledWith(payload);
  });

  it('should return FEEDBACK_SUCCESS action on success', () => {
    const payload = { navigationRating: 5 };
    spyOn(profileService, 'sendFeedback').and.callThrough();

    runner.queue(new actions.FeedbackAction(payload));
    profileEffects.feedback$
      .subscribe(result => {
        expect(result.type).toBe(actions.FEEDBACK_SUCCESS);
      });

    expect(profileService.sendFeedback).toHaveBeenCalledWith(payload);
  });

  it('should return FEEDBACK_FAILED action on fail', () => {
    const payload = { navigationRating: 5 };
    spyOn(profileService, 'sendFeedback')
      .and.returnValue(Observable.throw('Fake error'));

    runner.queue(new actions.FeedbackAction(payload));
    profileEffects.feedback$
      .subscribe(result => {
        expect(result.type).toBe(actions.FEEDBACK_FAILED);
      });

    expect(profileService.sendFeedback).toHaveBeenCalledWith(payload);
  });
});
