import { Action } from '@ngrx/store';

import { IFeedbackRating, IDataLog, IDataLogUpdate } from './profile.types';
import { IErrorAlert } from './../../models/error-alert';

export const FEEDBACK = '[Profile] Feedback';
export class FeedbackAction implements Action {
    readonly type = FEEDBACK;
    constructor(public payload: IFeedbackRating) {}
}

export const FEEDBACK_SUCCESS = '[Profile] Feedback Success';
export class FeedbackSuccessAction implements Action {
    readonly type = FEEDBACK_SUCCESS;
    constructor(public payload?: any ) {}
}

export const FEEDBACK_FAILED = '[Profile] Feedback Failed';
export class FeedbackFailedAction implements Action {
    readonly type = FEEDBACK_FAILED;
    constructor(public payload: IErrorAlert) {}
}

export const UPDATE_TEMPERATURE_UNIT = '[Profile] Update Temperature Unit';
export class UpdateTemperatureUnitAction implements Action {
  readonly type = UPDATE_TEMPERATURE_UNIT;
  constructor(public payload: string) {}
}

export const UPDATE_TEMPERATURE_UNIT_SUCCESS = '[Profile] Update Temperature Unit Success';
export class UpdateTemperatureUnitSuccessAction implements Action {
  readonly type = UPDATE_TEMPERATURE_UNIT_SUCCESS;
  constructor(public payload?: any) {}
}

export const UPDATE_TEMPERATURE_UNIT_FAILED = '[Profile] Update Temperature Unit Failed';
export class UpdateTemperatureUnitFailedAction implements Action {
  readonly type = UPDATE_TEMPERATURE_UNIT_FAILED;
  constructor(public payload: IErrorAlert) {}
}

export const GET_DATA_LOGS = '[Profile] Get Data Logs';
export class GetDataLogsAction implements Action {
  readonly type = GET_DATA_LOGS;
  constructor(public payload?: any) {}
}

export const GET_DATA_LOGS_SUCCESS = '[Profile] Get Data Logs Success';
export class GetDataLogsSuccessAction implements Action {
  readonly type = GET_DATA_LOGS_SUCCESS;
  constructor(public payload: Array<IDataLog>) {}
}

export const GET_DATA_LOGS_FAILED = '[Profile] Get Data Logs Failed';
export class GetDataLogsFailedAction implements Action {
  readonly type = GET_DATA_LOGS_FAILED;
  constructor(public payload: IErrorAlert) {}
}

export const CREATE_DATA_LOG = '[Profile] Create Data Log';
export class CreateDataLogAction implements Action {
  readonly type = CREATE_DATA_LOG;
  constructor(public payload: IDataLog) {}
}

export const CREATE_DATA_LOG_SUCCESS = '[Profile] Create Data Log Success';
export class CreateDataLogSuccessAction implements Action {
  readonly type = CREATE_DATA_LOG_SUCCESS;
  constructor(public payload?: any) {}
}

export const CREATE_DATA_LOG_FAILED = '[Profile] Create Data Log Error';
export class CreateDataLogFailedAction implements Action {
  readonly type = CREATE_DATA_LOG_FAILED;
  constructor(public payload: IErrorAlert) {}
}

export const UPDATE_DATA_LOG = '[Profile] Update Data Log';
export class UpdateDataLogAction implements Action {
  readonly type = UPDATE_DATA_LOG;
  constructor(public payload: IDataLogUpdate) {}
}

export const UPDATE_DATA_LOG_SUCCESS = '[Profile] Update Data Log Success';
export class UpdateDataLogSuccessAction implements Action {
  readonly type = UPDATE_DATA_LOG_SUCCESS;
  constructor(public payload?: any) {}
}

export const UPDATE_DATA_LOG_FAILED = '[Profile] Update Data Log Error';
export class UpdateDataLogFailedAction implements Action {
  readonly type = UPDATE_DATA_LOG_FAILED;
  constructor(public payload: IErrorAlert) {}
}

export const DISMISS_ERROR = '[Profile] Dismiss Error';
export class DismissErrorAction implements Action {
  readonly type = DISMISS_ERROR;
  constructor(public payload?: any) {}
}

export type Actions
  = UpdateTemperatureUnitAction
  | UpdateTemperatureUnitSuccessAction
  | UpdateTemperatureUnitFailedAction
  | FeedbackAction
  | FeedbackSuccessAction
  | FeedbackFailedAction
  | GetDataLogsAction
  | GetDataLogsSuccessAction
  | GetDataLogsFailedAction
  | CreateDataLogAction
  | CreateDataLogSuccessAction
  | CreateDataLogFailedAction
  | UpdateDataLogAction
  | UpdateDataLogSuccessAction
  | UpdateDataLogFailedAction
  | DismissErrorAction;

