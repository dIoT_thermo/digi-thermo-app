import { App } from 'ionic-angular';
import { Injectable } from '@angular/core';

import { Action } from '@ngrx/store';
import { Effect, Actions } from '@ngrx/effects';

import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';

import { ProfileService } from './../../providers/profile.service';
import * as profile from './profile.actions';
import * as authentication from './../authentication/authentication.actions';
import * as navigation from './../navigation/navigation.actions';
import { TabIndexes } from '../navigation/navigation.types';

@Injectable()
export class ProfileEffects {
  @Effect()
  updateTemperatureUnit$: Observable<Action> = this.actions$
    .ofType(profile.UPDATE_TEMPERATURE_UNIT)
    .switchMap((action: profile.UpdateTemperatureUnitAction) =>
      this.profileService.updateUnit(action.payload)
        .map((res) => new profile.UpdateTemperatureUnitSuccessAction(action.payload))
        .catch(error => Observable.of(new profile.UpdateTemperatureUnitFailedAction(error)))
    );

  @Effect()
  updateTemperatureUnitFB$: Observable<Action> = this.actions$
    .ofType(authentication.LOGIN_SUCCESS)
    .delay(1000)
    .switchMap((action: authentication.LoginSuccessAction) =>
      this.profileService.getUnitFromFirebase()
        .map((res) => new profile.UpdateTemperatureUnitSuccessAction(res.$value))
        .catch(error => Observable.of(new profile.UpdateTemperatureUnitFailedAction(error)))
    );

  @Effect()
  getDataLogs$: Observable<Action> = this.actions$
    .ofType(profile.GET_DATA_LOGS)
    .switchMap((action: profile.GetDataLogsAction) =>
      this.profileService.getDataLogs()
        .map(dataLogs => new profile.GetDataLogsSuccessAction(dataLogs))
        .catch(error => Observable.of(new profile.GetDataLogsFailedAction(error)))
    );

  @Effect()
  createDataLogs$: Observable<Action> = this.actions$
    .ofType(profile.CREATE_DATA_LOG)
    .switchMap((action: profile.CreateDataLogAction) =>
      this.profileService.createDataLog(action.payload)
        .switchMap(() => Observable.concat(
          Observable.of(new profile.CreateDataLogSuccessAction()),
          Observable.of(new navigation.ChangeTabAction(TabIndexes.dataLogTab)),
        ))
        .catch(error => Observable.of(new profile.CreateDataLogFailedAction(error)))
    );

  @Effect()
  updateDataLogs$: Observable<Action> = this.actions$
    .ofType(profile.UPDATE_DATA_LOG)
    .switchMap((action: profile.UpdateDataLogSuccessAction) =>
      this.profileService.updateDataLog(action.payload)
        .map(() => new profile.UpdateDataLogSuccessAction())
        .catch(error => Observable.of(new profile.UpdateDataLogFailedAction(error)))
    );

  @Effect()
  feedback$: Observable<Action> = this.actions$
      .ofType(profile.FEEDBACK)
      .switchMap((action: profile.FeedbackAction) =>
        this.profileService.sendFeedback(action.payload)
          .map(() => new profile.FeedbackSuccessAction())
          .do(() => {
            const feedbackModal = this.app.getActiveNav().getActive();
            feedbackModal.dismiss();
          })
          .catch(error => Observable.of(new profile.FeedbackFailedAction(error)))
      );

  constructor(
    private actions$: Actions,
    private profileService: ProfileService,
    private app: App
  ) {}
}
