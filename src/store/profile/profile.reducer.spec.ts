import * as profile from './profile.types';
import * as actions from './profile.actions';
import { reducer } from './profile.reducer';

describe('Profile Reducer', () => {
  let payload;
  let action: actions.Actions;
  let expectedState: profile.State;
  let newState;

  const initialState = profile.initialState;

  beforeEach(() => {
    expectedState = JSON.parse(JSON.stringify(initialState));
  });

  it('should handle UPDATE_TEMPERATURE_UNIT action', () => {
    payload = 'F';

    action = new actions.UpdateTemperatureUnitAction(payload);
    expectedState.isUpdating = true;
    expectedState.temperatureUnit = payload;

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle UPDATE_TEMPERATURE_UNIT_SUCCESS action', () => {
    payload = 'C';

    action = new actions.UpdateTemperatureUnitSuccessAction(payload);
    expectedState.isUpdating = false;
    expectedState.temperatureUnit = payload;
    expectedState.error = null,

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle UPDATE_TEMPERATURE_UNIT_FAILED action', () => {
    const message = 'Fake Error';
    payload = { message };
    action = new actions.UpdateTemperatureUnitFailedAction(payload);
    expectedState.error = payload;

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle FEEDBACK action', () => {
    const navigationRating = 5;
    payload = { navigationRating };
    action = new actions.FeedbackAction(payload);

    expectedState.isUpdating = true;

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle FEEDBACK_FAILED action', () => {
    action = new actions.FeedbackSuccessAction();
    expectedState.isUpdating = false;

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle FEEDBACK_FAILED action', () => {
    const message = 'Fake Error';
    payload = { message };
    action = new actions.FeedbackFailedAction(payload);

    expectedState.isUpdating = false;
    expectedState.error = payload;

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle GET_DATA_LOGS action', () => {
    action = new actions.GetDataLogsAction(payload);
    expectedState.isUpdating = true;

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle GET_DATA_LOGS_SUCCESS action', () => {
    const dataLog = { temeprature: 10 };
    payload = [dataLog];
    action = new actions.GetDataLogsSuccessAction(payload);
    expectedState.isUpdating = false;
    expectedState.dataLogs = payload;

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle GET_DATA_LOGS_FAILED action', () => {
    const message = 'Fake Error';
    payload = { message };
    action = new actions.GetDataLogsFailedAction(payload);
    expectedState.isUpdating = false;
    expectedState.error = payload;

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle UPDATE_DATA_LOG action', () => {
    const $key = 'fakeKey';
    const updates = { title: 'fake' };
    payload = { $key, updates };
    action = new actions.UpdateDataLogAction(payload);
    expectedState.isUpdating = true;

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle UPDATE_DATA_LOG_SUCCESS action', () => {
    action = new actions.UpdateDataLogSuccessAction();
    expectedState.isUpdating = false;

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle UPDATE_DATA_LOG_FAILED action', () => {
    const message = 'Fake Error';
    payload = { message };
    action = new actions.UpdateDataLogFailedAction(payload);
    expectedState.isUpdating = false;
    expectedState.error = payload;

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle DISMISS_ERROR action', () => {
    action = new actions.DismissErrorAction();
    expectedState.error = null;

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });
});
