import * as profile from './profile.actions';
import { State, initialState } from './profile.types';
import { IErrorAlert } from './../../models/error-alert';

export { State, initialState };

export function reducer(state = initialState, action: profile.Actions): State {
  const type = action.type;

  switch (type) {

    case profile.UPDATE_TEMPERATURE_UNIT: {
      const payload = (<profile.UpdateTemperatureUnitAction>action).payload;
      return {
        ...state,
        isUpdating: true,
        temperatureUnit: payload
      };
    }

    case profile.UPDATE_TEMPERATURE_UNIT_SUCCESS: {
      const payload = (<profile.UpdateTemperatureUnitSuccessAction>action).payload;
      return {
         ...state,
        isUpdating: false,
        temperatureUnit: payload,
        error: null,
      };
    }

    case profile.UPDATE_TEMPERATURE_UNIT_FAILED: {
      const payload = (<profile.UpdateTemperatureUnitFailedAction>action).payload;
      return {
        ...state,
        isUpdating: false,
        error: payload,
      };
    }

    case profile.GET_DATA_LOGS: {
      return {
        ...state,
        isUpdating: true,
      };
    }

    case profile.GET_DATA_LOGS_SUCCESS: {
      const payload = (<profile.GetDataLogsSuccessAction>action).payload;
      return {
        ...state,
        isUpdating: false,
        dataLogs: payload,
      };
    }

    case profile.GET_DATA_LOGS_FAILED: {
      const payload = (<profile.GetDataLogsFailedAction>action).payload;
      return {
        ...state,
        isUpdating: false,
        error: payload,
      };
    }

    case profile.CREATE_DATA_LOG: {
      return {
        ...state,
        isUpdating: true,
      };
    }

    case profile.CREATE_DATA_LOG_SUCCESS: {
      return {
        ...state,
        isUpdating: false,
      };
    }

    case profile.CREATE_DATA_LOG_FAILED: {
      const payload = (<profile.CreateDataLogFailedAction>action).payload;
      return {
        ...state,
        isUpdating: false,
        error: payload,
      };
    }

    case profile.UPDATE_DATA_LOG: {
      return {
        ...state,
        isUpdating: true,
      };
    }

    case profile.UPDATE_DATA_LOG_SUCCESS: {
      return {
        ...state,
        isUpdating: false,
      };
    }
    case profile.UPDATE_DATA_LOG_FAILED: {
      const payload = (<profile.UpdateDataLogFailedAction>action).payload;
      return {
        ...state,
        isUpdating: false,
        error: payload,
      };
    }

    case profile.DISMISS_ERROR: {
      return {
        ...state,
        error: null
      };
    }

    case profile.FEEDBACK: {
      const payload = (<profile.FeedbackAction>action).payload;
      return {
        ...state,
        isUpdating: true,
        error: null,
      };
    }

    case profile.FEEDBACK_SUCCESS: {
      const payload = (<profile.FeedbackSuccessAction>action).payload;
      return {
        ...state,
        isUpdating: false,
      };
    }

    case profile.FEEDBACK_FAILED: {
      const payload = (<profile.FeedbackFailedAction>action).payload;
      return {
        ...state,
        isUpdating: false,
        error: payload,
      };
    }

    default: {
      return state;
    }
  }
}

export const getIsUpdating = (state: State) => state.isUpdating;
export const getTemperatureUnit = (state: State) => state.temperatureUnit;
export const getDataLogs = (state: State) => state.dataLogs;
export const getError = (state: State) => state.error;
