import * as actions from './profile.actions';

import * as profile from './profile.types';
import { ModalService } from './../../providers/modal.service';

describe('Profile Actions', () => {
  let payload;
  let action: actions.Actions;

  it('should create UpdateTemperatureUnitAction', () => {
    payload = { temperatureUnit: 'F' };

    action = new actions.UpdateTemperatureUnitAction(payload);

    expect(action instanceof actions.UpdateTemperatureUnitAction).toBeTruthy();
    expect(action.type).toEqual(actions.UPDATE_TEMPERATURE_UNIT);
    expect(action.payload).toEqual(payload);
  });

  it('should create UpdateTemperatureUnitSuccessAction', () => {
    payload = { temperatureUnit: 'C'};

    action = new actions.UpdateTemperatureUnitSuccessAction(payload);

    expect(action instanceof actions.UpdateTemperatureUnitSuccessAction).toBeTruthy();
    expect(action.type).toEqual(actions.UPDATE_TEMPERATURE_UNIT_SUCCESS);
    expect(action.payload).toEqual(payload);
  });

  it('should create UpdateTemperatureUnitFailedAction', () => {
    const message = 'Fake Error';
    payload = { message };

    action = new actions.UpdateTemperatureUnitFailedAction(payload);

    expect(action instanceof actions.UpdateTemperatureUnitFailedAction).toBeTruthy();
    expect(action.type).toEqual(actions.UPDATE_TEMPERATURE_UNIT_FAILED);
    expect(action.payload).toEqual(payload);
  });

  it('should create GetDataLogsAction', () => {
    action = new actions.GetDataLogsAction();

    expect(action instanceof actions.GetDataLogsAction).toBeTruthy();
    expect(action.type).toEqual(actions.GET_DATA_LOGS);
    expect(action.payload).toBeUndefined();
  });

  it('should create GetDataLogsSuccessAction', () => {
    const dataLog = { temperature: 10 };
    payload = [dataLog];

    action = new actions.GetDataLogsSuccessAction(payload);

    expect(action instanceof actions.GetDataLogsSuccessAction).toBeTruthy();
    expect(action.type).toEqual(actions.GET_DATA_LOGS_SUCCESS);
    expect(action.payload).toEqual(payload);
  });

  it('should create GetDataLogsFailedAction', () => {
    const message = 'Fake Error';
    payload = { message };

    action = new actions.GetDataLogsFailedAction(payload);

    expect(action instanceof actions.GetDataLogsFailedAction).toBeTruthy();
    expect(action.type).toEqual(actions.GET_DATA_LOGS_FAILED);
    expect(action.payload).toEqual(payload);
  });

  it('should create UpdateDataLogAction', () => {
    const $key = 'fakeKey';
    const updates = { title: 'Fake' };
    payload = { $key, updates };

    action = new actions.UpdateDataLogAction(payload);

    expect(action instanceof actions.UpdateDataLogAction).toBeTruthy();
    expect(action.type).toEqual(actions.UPDATE_DATA_LOG);
    expect(action.payload).toEqual(payload);
  });

  it('should create UpdateDataLogSuccessAction', () => {
    action = new actions.UpdateDataLogSuccessAction();

    expect(action instanceof actions.UpdateDataLogSuccessAction).toBeTruthy();
    expect(action.type).toEqual(actions.UPDATE_DATA_LOG_SUCCESS);
    expect(action.payload).toBeUndefined();
  });

  it('should create UpdateDataLogFailedAction', () => {
    const message = 'Fake Error';
    payload = { message };

    action = new actions.UpdateDataLogFailedAction(payload);

    expect(action instanceof actions.UpdateDataLogFailedAction).toBeTruthy();
    expect(action.type).toEqual(actions.UPDATE_DATA_LOG_FAILED);
    expect(action.payload).toEqual(payload);
  });

  it('should create DismissErrorAction', () => {
    action = new actions.DismissErrorAction();

    expect(action instanceof actions.DismissErrorAction).toBeTruthy();
    expect(action.type).toEqual(actions.DISMISS_ERROR);
    expect(action.payload).toBeUndefined();
  });

  it('should create FeedbackAction', () => {
    const usabilityRating = '5';
    const usabilityComments = 'Intuitive!';
    const designRating = '5';
    const designComments = 'Beautiful!';
    const additionalComments = 'I just love it!';
    payload = { usabilityRating, usabilityComments, designRating, designComments, additionalComments };

    action = new actions.FeedbackAction(payload);

    expect(action instanceof actions.FeedbackAction).toBeTruthy();
    expect(action.type).toEqual(actions.FEEDBACK);
    expect(action.payload).toEqual(payload);
  });

  it('should create FeedbackSuccessAction', () => {
    action = new actions.FeedbackSuccessAction();

    expect(action instanceof actions.FeedbackSuccessAction).toBeTruthy();
    expect(action.type).toEqual(actions.FEEDBACK_SUCCESS);
  });

  it('should create FeedbackFailedAction', () => {
    const message = 'Fake Error';
    payload = { message };

    action = new actions.FeedbackFailedAction(payload);

    expect(action instanceof actions.FeedbackFailedAction).toBeTruthy();
    expect(action.type).toEqual(actions.FEEDBACK_FAILED);
    expect(action.payload).toEqual(payload);
  });
});

