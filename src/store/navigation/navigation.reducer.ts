import * as navigation from './navigation.actions';
import { State, initialState } from './navigation.types';
import { IFirebaseUserProfile } from './../authentication/authentication.types';


export { State, initialState };

export function reducer(state = initialState, action: navigation.Actions): State {
  const type = action.type;

  switch (type) {
    case navigation.CHANGE_STACK: {
      const payload = (<navigation.ChangeStackAction>action).payload;

      return { ...state, currentStack: payload };
    }

    case navigation.PUSH: {
      const payload = (<navigation.PushAction>action).payload;

      const stack = state.currentStack;
      const updatedStack = [...state[stack]];
      updatedStack.push(payload);

      return { ...state, [stack]: updatedStack };
    }

    case navigation.POP: {
      const stack = state.currentStack;
      const updatedStack = [...state[stack]];
      updatedStack.pop();

      return { ...state, [stack]: updatedStack };
    }

    case navigation.OPEN_MODAL: {
      const payload = (<navigation.OpenModalAction>action).payload;

      const openedModals = [...state.openedModals];
      openedModals.push(payload);

      return { ...state, openedModals };
    }

    case navigation.CLOSE_MODAL: {
      const payload = (<navigation.OpenModalAction>action).payload;

      const openedModals = [...state.openedModals];
      const idx = openedModals.findIndex(m => m === payload);

      if (idx > -1) {
        openedModals.splice(idx, 1);
      }

      return { ...state, openedModals };
    }

    default: {
      return state;
    }
  }
}
