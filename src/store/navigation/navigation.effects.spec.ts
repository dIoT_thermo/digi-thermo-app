import { TestBed, fakeAsync, tick } from '@angular/core/testing';
import {} from 'jasmine';

import { App, NavController } from 'ionic-angular';

import { EffectsTestingModule, EffectsRunner } from '@ngrx/effects/testing';

import * as actions from './navigation.actions';
import * as navigation from './navigation.types';
import { NavigationEffects } from './navigation.effects';
import { ModalService } from './../../providers/modal.service';
import { ModalServiceMock } from './../../mocks/modal.service.mock';
import { AppMock } from './../../mocks/ionic/app.mock';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { InAppBrowserMock } from './../../mocks/ionic-native/in-app-browser.mock';
import { Platform } from 'ionic-angular';
import { PlatformMock } from './../../mocks/ionic/platform.mock';

describe('NavigationEffects', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      EffectsTestingModule
    ],
    providers: [
      NavigationEffects,
      {
        provide: ModalService,
        useClass: ModalServiceMock
      },
      {
        provide: App,
        useClass: AppMock
      },
      {
        provide: InAppBrowser,
        useClass: InAppBrowserMock
      },
      {
        provide: Platform,
        useClass: PlatformMock
      }
    ]
  }));

  let runner: EffectsRunner;
  let navigationEffects: NavigationEffects;
  let navCtrl: NavController;
  let modalService: ModalService;
  let platform: Platform;
  let inAppBrowser: InAppBrowser;

  beforeEach(() => {
    runner = TestBed.get(EffectsRunner);
    navigationEffects = TestBed.get(NavigationEffects);
    navCtrl = TestBed.get(App).getActiveNav();
    modalService = TestBed.get(ModalService);
    platform = TestBed.get(Platform);
    inAppBrowser = TestBed.get(InAppBrowser);
  });

  it('should return CHANGE_STACK & PUSH actions after PUSH_ONTO_STACK action', () => {
    const page = { name: 'TestPage' };
    const stack = navigation.TABS[navigation.TabIndexes.dataLogTab].stack;
    const payload = { page, stack };
    let actionCount = 0;

    runner.queue(new actions.PushOntoAction(payload));
    navigationEffects.pushOnto$
    .subscribe(result => {
      if (actionCount === 0) {
        expect(result.type).toBe(actions.CHANGE_STACK);
        expect(result.payload).toEqual(payload.stack);
      } else if (actionCount === 1) {
        expect(result.type).toBe(actions.PUSH);
        expect(result.payload).toEqual(payload.page);
      }

      actionCount++;
    });

    expect(actionCount).toBe(2);
  });

  it('should change NavController root page on PUSH when specified', () => {
    const payload = { name: 'TestPage', options: { setRoot: true } };
    spyOn(navCtrl, 'setRoot');
    spyOn(navCtrl, 'push');

    runner.queue(new actions.PushAction(payload));
    navigationEffects.push$
    .subscribe(() => {}); // Subscribe to flush queue

    expect(navCtrl.setRoot).toHaveBeenCalledWith(payload.name);
    expect(navCtrl.push).not.toHaveBeenCalled();
  });

  it('should push a page to NavController on PUSH', () => {
    const payload = { name: 'TestPage' };
    spyOn(navCtrl, 'push');
    spyOn(navCtrl, 'setRoot');

    runner.queue(new actions.PushAction(payload));
    navigationEffects.push$
    .subscribe(() => {}); // Subscribe to flush queue

    expect(navCtrl.push).toHaveBeenCalledWith(payload.name);
    expect(navCtrl.setRoot).not.toHaveBeenCalled();
  });

  it('should pop a page off NavController on POP', () => {
    spyOn(navCtrl, 'pop');

    runner.queue(new actions.PopAction());
    navigationEffects.pop$
    .subscribe(() => {}); // Subscribe to flush queue

    expect(navCtrl.pop).toHaveBeenCalled();
  });

  it('should change selected NavController and fire PUSH_ONTO_STACK on CHANGE_TAB', () => {
    const payload = navigation.TabIndexes.settingsTab;
    spyOn(navCtrl.parent, 'select');

    runner.queue(new actions.ChangeTabAction(payload));
    navigationEffects.changeTab$
    .subscribe(result => {
      expect(result.type).toEqual(actions.CHANGE_STACK);
      expect(result.payload).toEqual(navigation.TABS[payload].stack);
    });

    expect(navCtrl.parent.select).toHaveBeenCalledWith(payload);
  });

  it('should call ModalService#openModal on OPEN_MODAL', () => {
    const payload = 'TestModalComponent';
    spyOn(modalService, 'openModal');

    runner.queue(new actions.OpenModalAction(payload));
    navigationEffects.openModal$
    .subscribe(() => {}); // Subscribe to flush queue

    expect(modalService.openModal).toHaveBeenCalledWith(payload);
  });

  it('should create an instance of InAppBrowser', fakeAsync(() => {
    const payload = 'http://www.yahoo.com';

    spyOn(inAppBrowser, 'create');
    spyOn(platform, 'ready').and.callThrough();

    runner.queue(new actions.OpenBrowserAction(payload));
    navigationEffects.openBrowser$
    .subscribe(() => {});
    tick();

    expect(inAppBrowser.create).toHaveBeenCalledWith(payload, '_blank', 'location=no');
  }));
});
