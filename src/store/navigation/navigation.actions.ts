  import { Action } from '@ngrx/store';

  import { INavTab, TabIndexes, CurrentStack, IPushOntoActionPayload, INavPage } from './navigation.types';

  export const PUSH_ONTO_STACK = '[Navigation] Push Page Onto Stack';
  export class PushOntoAction implements Action {
    readonly type = PUSH_ONTO_STACK;
    constructor(public payload: IPushOntoActionPayload) {}
  }

  export const CHANGE_STACK = '[Navigation] Change Nav Stack';
  export class ChangeStackAction implements Action {
    readonly type = CHANGE_STACK;
    constructor(public payload: CurrentStack) {}
  }

  export const PUSH = '[Navigation] Push Page';
  export class PushAction implements Action {
    readonly type = PUSH;
    constructor(public payload: INavPage) {}
  }

  export const POP = '[Navigation] Pop Page';
  export class PopAction implements Action {
    readonly type = POP;
    constructor(public payload?: any) {}
  }

  export const CHANGE_TAB = '[Navigation] Change Tab';
  export class ChangeTabAction implements Action {
    readonly type = CHANGE_TAB;
    constructor(public payload: number) {}
  }

  export const OPEN_MODAL = '[Navigation] Open Modal';
  export class OpenModalAction implements Action {
    readonly type = OPEN_MODAL;
    constructor(public payload: string) {}
  }

  export const CLOSE_MODAL = '[Navigation] Did Close Modal';
  export class CloseModalAction implements Action {
    readonly type = CLOSE_MODAL;
    constructor(public payload: string) {}
  }

  export const OPEN_BROWSER = '[Navigation] Did Open Browser';
  export class OpenBrowserAction implements Action {
    readonly type = OPEN_BROWSER;
    constructor(public payload: string) {}
  }

  export type Actions
    = PushOntoAction
    | ChangeStackAction
    | PushAction
    | PopAction
    | ChangeTabAction
    | OpenModalAction
    | CloseModalAction
    | OpenBrowserAction;
