import * as actions from './navigation.actions';
import * as navigation from './navigation.types';
import { ModalService } from './../../providers/modal.service';

describe('Navigation Actions', () => {
  let payload;
  let action: actions.Actions;

  it('should create PushOntoAction', () => {
    const page = { name: 'TestPage' };
    const stack = navigation.TABS[navigation.TabIndexes.dataLogTab].stack;
    payload = { page, stack };

    action = new actions.PushOntoAction(payload);

    expect(action instanceof actions.PushOntoAction).toBeTruthy();
    expect(action.type).toEqual(actions.PUSH_ONTO_STACK);
    expect(action.payload).toEqual(payload);
  });

  it('should create ChangeStackAction', () => {
    payload = 'rootStack';

    action = new actions.ChangeStackAction(payload);

    expect(action instanceof actions.ChangeStackAction).toBeTruthy();
    expect(action.type).toEqual(actions.CHANGE_STACK);
    expect(action.payload).toEqual(payload);
  });

  it('should create PushAction', () => {
    payload = { name: 'TestPage' };

    action = new actions.PushAction(payload);

    expect(action instanceof actions.PushAction).toBeTruthy();
    expect(action.type).toEqual(actions.PUSH);
    expect(action.payload).toEqual(payload);
  });

  it('should create PopAction', () => {
    action = new actions.PopAction();

    expect(action instanceof actions.PopAction).toBeTruthy();
    expect(action.type).toEqual(actions.POP);
    expect(action.payload).toBeUndefined();
  });

  it('should create CreateTabAction', () => {
    payload = navigation.TabIndexes.dataLogTab;

    action = new actions.ChangeTabAction(payload);

    expect(action instanceof actions.ChangeTabAction).toBeTruthy();
    expect(action.type).toEqual(actions.CHANGE_TAB);
    expect(action.payload).toEqual(payload);
  });

  it('should create OpenModalAction', () => {
    payload = 'TestModalComponent';

    action = new actions.OpenModalAction(payload);

    expect(action instanceof actions.OpenModalAction);
    expect(action.type).toEqual(actions.OPEN_MODAL);
    expect(action.payload).toEqual(payload);
  });

  it('should create CloseModalAction', () => {
    payload = 'TestModalComponent';

    action = new actions.CloseModalAction(payload);

    expect(action instanceof actions.CloseModalAction);
    expect(action.type).toEqual(actions.CLOSE_MODAL);
    expect(action.payload).toEqual(payload);
  });

  it('should create ViewTermsAction', () => {
    payload = 'http://www.yahoo.com/';

    action = new actions.OpenBrowserAction(payload);

    expect(action instanceof actions.OpenBrowserAction);
    expect(action.type).toEqual(actions.OPEN_BROWSER);
    expect(action.payload).toEqual(payload);
  });
});
