import * as navigation from './navigation.types';
import * as actions from './navigation.actions';
import { reducer } from './navigation.reducer';

describe('Navigation Reducer', () => {
  let payload;
  let action;
  let expectedState: navigation.State;
  let newState;

  const initialState = navigation.initialState;

  beforeEach(() => {
    expectedState = JSON.parse(JSON.stringify(initialState));
  });

  it('should return initial state for unhandled actions', () => {
    action = <any>'fakeAction';

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle CHANGE_STACK action', () => {
    payload = navigation.TABS[navigation.TabIndexes.dataLogTab].stack;
    action = new actions.ChangeStackAction(payload);
    expectedState.currentStack = payload;

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle PUSH action', () => {
    payload = { page: 'TestPage' };
    action = new actions.PushAction(payload);
    expectedState.rootStack.push(payload);

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle POP action', () => {
    action = new actions.PopAction();
    const newInitialState: navigation.State = JSON.parse(JSON.stringify(initialState));
    newInitialState.rootStack.push({ name: 'TestPage' });

    newState = reducer(newInitialState, action);
    expect(newState.rootStack).toEqual([]);
  });

  it('should handle OPEN_MODAL action', () => {
    payload = 'TestModalComponent';
    action = new actions.OpenModalAction(payload);
    expectedState.openedModals.push(payload);

    newState = reducer(undefined, action);

    expect(newState).toEqual(expectedState);
  });

  it('should handle CLOSE_MODAL action', () => {
    payload = 'TestModalComponent';
    action = new actions.CloseModalAction(payload);
    const newInitialState: navigation.State = JSON.parse(JSON.stringify(initialState));
    newInitialState.openedModals.push(payload);

    newState = reducer(newInitialState, action);

    expect(newState.openedModals).toEqual([]);
  });
});
