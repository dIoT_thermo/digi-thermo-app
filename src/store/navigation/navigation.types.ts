import { NavOptions } from 'ionic-angular';

export type CurrentStack = 'rootStack' | 'dataLogTab' | 'thermometerTab' | 'settingsTab';

export interface INavTab {
  root: string;
  title: string;
  icon: string;
  stack: CurrentStack;
}

interface INavOptionsExtended extends NavOptions {
  setRoot: boolean;
}

export interface INavPage {
  name: string;
  params?: { [key: string]: any };
  options?: INavOptionsExtended;
}

export interface IPushOntoActionPayload {
  page: INavPage;
  stack: CurrentStack;
}

export enum TabIndexes {
  dataLogTab = 0,
  thermometerTab = 1,
  settingsTab = 2,
}

const TABS: Array<INavTab> = [];
TABS[TabIndexes.dataLogTab] = { root: 'DataLogPage',
  title: 'Data Log', icon: 'stats',
  stack: TabIndexes[TabIndexes.dataLogTab] as CurrentStack };
TABS[TabIndexes.thermometerTab] = { root: 'ThermometerPage',
  title: 'Thermometer', icon: 'thermometer',
  stack: TabIndexes[TabIndexes.thermometerTab] as CurrentStack };
TABS[TabIndexes.settingsTab] = { root: 'SettingsPage',
  title: 'Settings', icon: 'settings',
  stack: TabIndexes[TabIndexes.settingsTab] as CurrentStack };
export { TABS };

export interface State {
  rootStack: Array<INavPage>;
  dataLogTab: Array<INavPage>;
  thermometerTab: Array<INavPage>;
  settingsTab: Array<INavPage>;
  currentStack: CurrentStack;
  openedModals: Array<string>;
}

export const initialState: State = {
  rootStack: [],
  dataLogTab: [],
  thermometerTab: [],
  settingsTab: [],
  currentStack: 'rootStack',
  openedModals: [],
};
