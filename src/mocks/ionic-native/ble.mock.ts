import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/takeUntil';

function hexToBytes(hex) {
  const bytes = [];
  for (let c = 0; c < hex.length; c += 2) {
    bytes.push(parseInt(hex.substr(c, 2), 16));
  }
  return bytes;
}

export class BLEMock {
  static SUCCESS_ENABLED = ['success'];
  static FAILED_ENABLED = ['failed'];
  static SUCCESS_DEVICE_LIST = ['success'];
  static FAILED_DEVICE_LIST = ['failed'];
  static SUCCESS_CONNECT = ['success'];
  static FAILED_CONNECT = ['failed'];

  stopScan$: Subject<boolean> = new Subject();

  isEnabled() {
      return Promise.resolve();
  }

  isConnected() {
    return Promise.resolve();
  }

  enable() {
    return Promise.resolve();
  }

  startScan() {
    let id = 1;
    return Observable.interval(1000)
      .takeUntil(this.stopScan$.asObservable())
      .map(() => ({ name: `HumiTemp Sensor Tag`, id}))
      .do(() => id++);
  }

  stopScan() {
    this.stopScan$.next(true);
    return Promise.resolve();
  }

  connect(id) {
    return Observable.of({ id });
  }

  startNotification() {
    const getDecimal = () => Math.random() * (99 - 0) + 0;
    const getTemp = () => Math.random() * (199 + 99) - 99;
    const getHumidity = () => Math.random() * (100 - 1) + 1;

    return Observable.interval(1000)
      .map(() => {
        const sign = Math.round(Math.random());
        const temperature = getTemp().toString(16) + getDecimal().toString(16);
        const humidity = getHumidity().toString(16) + getDecimal().toString(16);

        const hex = `${sign}${temperature}00${humidity}`;

        return hexToBytes(hex);
      });
  }

  writeWithoutResponse() {
    return Promise.resolve();
  }

  write() {
    return Promise.resolve();
  }

  disconnect() {
    return Promise.resolve();
  }
}
