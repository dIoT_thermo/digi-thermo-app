import { GeolocationOptions, Geoposition, Coordinates } from '@ionic-native/geolocation';

export class GeolocationMock {
  static fakeCoords: Coordinates = {
    latitude: 10.01,
    longitude: 10.01,
    accuracy: 100,
    altitude: 10,
    altitudeAccuracy: 10,
    heading: 10,
    speed: 10,
  };

  getCurrentPosition(options?: GeolocationOptions): Promise<Geoposition> {
    return Promise.resolve({
      coords: GeolocationMock.fakeCoords,
      timestamp: 100000,
    });
  }
}
