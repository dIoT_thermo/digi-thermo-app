export class FacebookMock {
  static SUCCESSFUL_LOGIN = ['success'];
  static FAILED_LOGIN = ['fail'];

  login(permissions: string[]): Promise<any> {
    if (permissions === FacebookMock.FAILED_LOGIN) {
      return Promise.reject({ code: 'fake', message: 'fake' });
    }
    return Promise.resolve({
    authResponse: { accessToken: 'success' }
  });
  }

  logout() {
    return Promise.resolve();
  }
}
