import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { ILoginCredentials } from './../store/authentication/authentication.types';

export class AuthServiceMock {
  signUp() {
    return Observable.of({});
  }
  login() {
    return Observable.of({});
  }

  facebookAuth() {
    return Observable.of({});
  }

  resetPassword() {
    return Observable.of({});
  }

  logout() {
    return Observable.of(true);
  }

  updateGeolocation() {

  }

  checkAuthState() {
    return Observable.of({});
  }
}
