export class LoadingControllerMock {
  create() {
    const loading = {};
    loading['present'] = (() => true);
    loading['dismiss'] = () => new Promise((resolve) => resolve());

    return loading;
  }
}
