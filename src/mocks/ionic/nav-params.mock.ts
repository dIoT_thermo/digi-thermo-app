export class NavParamsMock {
  static returnParam = null;

  static setParams(value) {
    NavParamsMock.returnParam = value;
  }
  public get(key): any {
    if (NavParamsMock.returnParam) {
      return NavParamsMock.returnParam;
    }
    return 'default';
  }
}
