export class ToastControllerMock {
  create() {
    const create = {};
    create['present'] = (() => true);

    return create;
  }
}
