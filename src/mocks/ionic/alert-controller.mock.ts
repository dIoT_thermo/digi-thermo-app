export class AlertControllerMock {
  create() {
    const alert = {};
    alert['present'] = (() => true);
    alert['dismiss'] = () => new Promise((resolve) => resolve());
    alert['onDidDismiss'] = () => new Promise((resolve) => resolve());

    return alert;
  }
}
