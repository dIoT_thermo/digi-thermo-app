import { FirebaseListObservable } from 'angularfire2/database';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import { IDataLog } from './../../store/profile/profile.types';

const fakeDataLogs: Array<IDataLog> = [{
  $key: 'fake',
  comment: 'fake',
  title: 'fake',
  humidity: 50,
  temperature: 70,
  createdAt: 1000000,
}];

class ReferenceMock {
  set(value: any) {
    return Promise.resolve();
  }
}

class DatabaseMock {
  ref(path) {
    return new ReferenceMock();
  }
}

export class AngularFireDatabaseMock {
  static pushSpy: jasmine.Spy = jasmine.createSpy('push');

  static updateSpy: jasmine.Spy = jasmine.createSpy('update')
    .and.callFake((key: string, value: any) => {
      if (key === 'valid') {
        return Promise.resolve();
      }

      return Promise.reject('Fake Error');
    });

  static setSpy: jasmine.Spy = jasmine.createSpy('set')
    .and.callFake((value: any) => {
      if (value === 'valid') {
        return Promise.resolve();
      }

      return Promise.reject('Fake Error');
    });

  static listSpy: jasmine.Spy = jasmine.createSpy('list')
    .and.callFake((path: string) => {
      let observable;
      if (path.includes('valid')) {
        observable = Observable.of(fakeDataLogs);
        observable.push = AngularFireDatabaseMock.pushSpy;
        observable.update = AngularFireDatabaseMock.updateSpy;
      } else {
        observable = Observable.throw({ message: 'Fake Error' });
      }

      return observable;
    });

  list = AngularFireDatabaseMock.listSpy;
  app = {
    database: () => new DatabaseMock()
  };

  object = () => createFirebaseObjectObservable();
}

function createFirebaseObjectObservable() {
  let observable;
  observable = Observable.of({});
  observable.push = AngularFireDatabaseMock.pushSpy;
  observable.update = AngularFireDatabaseMock.updateSpy;
  observable.set = AngularFireDatabaseMock.setSpy;
  return observable;
}
