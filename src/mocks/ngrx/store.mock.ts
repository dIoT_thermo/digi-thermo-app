import { Action } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { map } from 'rxjs/operator/map';

export class StoreMock<S> extends BehaviorSubject<S> {
  constructor(private _initialState: S) {
    super(_initialState);
  }

  dispatch(action: Action): void {

  }

  select<T, U>(pathOrMapFn: any, ...paths: string[]): Observable<U> {
    return map.call(this, pathOrMapFn);
  }
}
