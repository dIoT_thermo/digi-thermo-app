import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

export class ProfileServiceMock {
  getDataLogs() {
    return Observable.of({});
  }
  updateDataLog() {
    return Observable.of({});
  }

  updateUnit() {
    return Observable.of({});
  }

  sendFeedback() {
    return Observable.of({});
  }
}
