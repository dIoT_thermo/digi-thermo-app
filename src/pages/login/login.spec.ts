import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';

import { IonicModule, AlertController, LoadingController, Alert, Loading } from 'ionic-angular';

import { StoreModule, Store } from '@ngrx/store';

import { Subscription } from 'rxjs/Subscription';

import { LoginPage } from './login';
import { State as RootState, reducer, initialState } from './../../store/app.reducer';
import { AlertControllerMock } from './../../mocks/ionic/alert-controller.mock';
import { LoadingControllerMock } from './../../mocks/ionic/loading-controller.mock';

import { StoreMock } from './../../mocks/ngrx/store.mock';
import * as authentication from './../../store/authentication/authentication.actions';
import * as navigation from './../../store/navigation/navigation.actions';
import * as fromRoot from './../../store/app.reducer';

describe('LoginPage', () => {
  let _store: Store<RootState>;
  let fixture: ComponentFixture<LoginPage>;
  let component: LoginPage;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginPage],
      imports: [
        IonicModule.forRoot(LoginPage),
        StoreModule.provideStore(reducer)
      ],
      providers: [
        FormBuilder,
        {
          provide: AlertController,
          useClass: AlertControllerMock,
        },
        {
          provide: LoadingController,
          useClass: LoadingControllerMock,
        },
        {
          provide: Store,
          useValue: new StoreMock(initialState),
        }
      ],
    });
  }));

  beforeEach(() => {
    _store = TestBed.get(Store);
    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    de = fixture.debugElement;
  });

  it('should be created', () => {
    expect(component instanceof LoginPage).toBeTruthy();
  });

  describe('Properties', () => {
    describe('loginForm', () => {
      it('should have two input fields', () => {
        const controls = Object.keys(component.loginForm.controls);
        expect(controls.length).toBe(2);
      });

      describe('Email Input', () => {
        let formControl: FormControl;
        beforeEach(() => {
          formControl = component.loginForm.controls['email'] as FormControl;
        });

        it('should exist', () => {
          expect(formControl).toBeDefined();
        });

        it('should be required', () => {
          const errors = formControl.errors || {};

          expect(errors['required']).toBeTruthy();
        });

        it('should match angular\'s email pattern', () => {
          let errors = {};
          formControl.setValue('test');
          errors = formControl.errors || {};

          expect(errors['email']).toBeTruthy();

          formControl.setValue('test@valid');
          errors = formControl.errors || {};

          expect(errors['email']).toBeFalsy();

          formControl.setValue('invalid@test.');
          errors = formControl.errors || {};

          expect(errors['email']).toBeTruthy();

          formControl.setValue('valid@test.com');
          errors = formControl.errors || {};

          expect(errors['email']).toBeFalsy();
        });
      });

      describe('Password Input', () => {
        let formControl: FormControl;
        beforeEach(() => {
          formControl = component.loginForm.controls['password'] as FormControl;
        });

        it('should exist', () => {
          expect(formControl).toBeDefined();
        });

        it('should be required', () => {
          const errors = formControl.errors || {};

          expect(errors['required']).toBeTruthy();
        });

        it('should have a minimum length of 6', () => {
          let errors = formControl.errors || {};
          formControl.setValue('test');
          errors = formControl.errors || {};

          expect(errors['minlength']).toBeTruthy();

          formControl.setValue('123test');
          errors = formControl.errors || {};

          expect(errors['minlength']).toBeFalsy();
        });
      });

      describe('On Submit', () => {
        it('should dispatch a new authentication.LoginAction', () => {
          const fakeCredentials = {
            email: 'fake@email.com',
            password: '123test',
          };
          expect(component.loginForm.valid).toBeFalsy();

          component.loginForm.controls['email'].setValue(fakeCredentials.email);
          component.loginForm.controls['password'].setValue(fakeCredentials.password);
          expect(component.loginForm.valid).toBeTruthy();
          spyOn(_store, 'dispatch').and.callFake((action) => {
            expect(action instanceof authentication.LoginAction).toBeTruthy();
            expect(action.type).toBe(authentication.LOGIN);
            expect(action.payload).toEqual(fakeCredentials);
          });

          component.login();
        });
      });
    });

    describe('disableLoginButton$', () => {
      it('should be updated on login form status changes', () => {
        const emailInput = component.loginForm.controls['email'];
        const passwordInput = component.loginForm.controls['password'];

        let testCase = 0;
        component.disableLoginButton$.subscribe((disable) => {
          // Initial value
          if (testCase === 0) {
            expect(disable).toBeTruthy();
          }

          // Invalid Form
          if (testCase === 1) {
            expect(disable).toBeTruthy();
          }

          // Valid Form
          if (testCase === 2) {
            expect(disable).toBeFalsy();
          }
        });

        // Invalid Form
        testCase++;
        emailInput.setValue('valid@email.com');

        // Valid Form
        testCase++;
        passwordInput.setValue('validPassword');
      });
    });

    describe('isLoading$', () => {
      it('should be created by selecting isLoading from authentication state', () => {
        const expected = _store.select(fromRoot.getAuthIsLoading);

        expect(component.isLoading$).toEqual(expected);
      });
    });

    describe('authError$', () => {
      it('should be created by selecting error from authentication state', () => {
        const expected = _store.select(fromRoot.getAuthError);

        expect(component.authError$).toEqual(expected);
      });
    });

    describe('loadingSpinner', () => {
      it('should be undefined by default', () => {
        expect(component.loadingSpinner).toBeUndefined();
      });
    });

    describe('alert', () => {
      it('should be undefined by default', () => {
        expect(component.alert).toBeUndefined();
      });
    });
  });

  describe('Methods', () => {
    describe('ionViewWillEnter()', () => {
      it('should subscribe to isLoading$', () => {
        spyOn(component.isLoading$, 'subscribe');

        component.ionViewWillEnter();

        expect(component.isLoading$.subscribe).toHaveBeenCalled();
      });

      it('should subscribe to authError$', () => {
        spyOn(component.authError$, 'subscribe');

        component.ionViewWillEnter();

        expect(component.authError$.subscribe).toHaveBeenCalled();
      });

      it('should add two subscriptions in total', () => {
        component.ionViewWillEnter();

        expect(component.subscriptions instanceof Subscription).toBeTruthy();
        expect(component.subscriptions['_subscriptions'].length).toBe(2);
      });
    });

    describe('ionViewWillLeave()', () => {
      beforeEach(() => {
        component.ionViewWillEnter();
      });

      it('should unsubscribe from observables', () => {
        spyOn(component.subscriptions, 'unsubscribe');

        component.ionViewWillLeave();

        expect(component.subscriptions.unsubscribe).toHaveBeenCalled();
      });

      it('should dismiss any present alerts', () => {
        component.alert = <Alert>{dismiss() {}};
        spyOn(component.alert, 'dismiss');

        component.ionViewWillLeave();

        expect(component.alert.dismiss).toHaveBeenCalled();
      });
    });

    describe('facebookLogin()', () => {
      it('should dispatch a new authentication.FacebookLoginAction', () => {
        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof authentication.FacebookLoginAction).toBeTruthy();
          expect(action.type).toBe(authentication.FACEBOOK_LOGIN);
        });

        component.facebookLogin();
      });
    });

    describe('login()', () => {
      it('should dispatch a new authentication.LoginAction', () => {
        const fakeCredentials = {
          email: 'fake@email.com',
          password: '123test',
        };
        component.loginForm.setValue(fakeCredentials);

        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof authentication.LoginAction).toBeTruthy();
          expect(action.type).toBe(authentication.LOGIN);
          expect(action.payload).toEqual(fakeCredentials);
        });

        component.login();
      });
    });

    describe('forgotPassword()', () => {
      it('should dispatch a navigation.PushAction to ForgotPasswordPage', () => {
        const page = { name: 'ForgotPasswordPage' };

        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof navigation.PushAction).toBeTruthy();
          expect(action.type).toBe(navigation.PUSH);
          expect(action.payload).toEqual(page);
        });

        component.forgotPassword();
      });
    });

    describe('signUp()', () => {
      it('should dispatch a navigation.PushAction to RegisterPage', () => {
        const page = { name: 'RegisterPage' };

        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof navigation.PushAction).toBeTruthy();
          expect(action.type).toBe(navigation.PUSH);
          expect(action.payload).toEqual(page);
        });

        component.signUp();
      });
    });

    describe('handleLoading()', () => {
      it('should dismiss any present spinner when state isLoading is false', () => {
        component.loadingSpinner = <Loading>{ dismiss() {} };
        spyOn(component.loadingSpinner, 'dismiss');

        component.handleLoading(false);

        expect(component.loadingSpinner.dismiss).toHaveBeenCalled();
      });

      it('should create a loading spinner when state isLoading is true', () => {
        spyOn(component.loadingCtrl, 'create').and.callThrough();

        component.handleLoading(true);

        expect(component.loadingCtrl.create).toHaveBeenCalled();
      });
    });

    describe('handleAuthError()', () => {
      it('should create an alert when passed an error', () => {
        const fakeError = {
          title: 'Alert!',
          message: 'Random Message',
          buttons: ['Fake', 'Buttons']
        };
        spyOn(component.alertCtrl, 'create').and.callThrough();

        component.handleAuthError(fakeError);

        expect(component.alertCtrl.create).toHaveBeenCalled();
      });
    });
  });

  describe('Template Integration', () => {
    const fakeCredentials = {
      email: 'fake@email.com',
      password: '123test',
    };

    beforeEach(() => {
      fixture.detectChanges();
    });

    it('should call facebookLogin() on facebook button click', () => {
      const button = de.query(By.css('button.button-facebook'));
      spyOn(component, 'facebookLogin');

      button.triggerEventHandler('click', null);

      expect(component.facebookLogin).toHaveBeenCalled();
    });

    describe('login button state', () => {
      let button: HTMLButtonElement;

      beforeEach(() => {
        button = de.query(By.css('button[type="submit"]')).nativeElement;
      });

      // skipping - not a requirement
      xit('should be disabled when the form is invalid', () => {
        expect(button.disabled).toBeTruthy();
        component.loginForm.controls['email'].setValue('invalidEmail');

        fixture.detectChanges();

        expect(button.disabled).toBeTruthy();
      });

      // skipping - not a requirement
      xit('should be enabled when the form is valid', () => {
        expect(button.disabled).toBeTruthy();
        component.loginForm.setValue(fakeCredentials);

        fixture.detectChanges();

        expect(button.disabled).toBeFalsy();
      });
    });

    it('should call login() on form submit', () => {
      const formEl = de.query(By.css('form'));
      component.loginForm.setValue(fakeCredentials);
      spyOn(component, 'login');

      fixture.detectChanges();
      formEl.triggerEventHandler('submit', null);

      expect(component.login).toHaveBeenCalled();
    });

    it('should call forgotPassword() on forgot password button click', () => {
      const formButtons = de.queryAll(By.css('form button[type="button"]'));
      const button = formButtons[formButtons.length - 2];
      spyOn(component, 'forgotPassword');

      button.triggerEventHandler('click', null);

      expect(component.forgotPassword).toHaveBeenCalled();
    });

    it('should call signUp() on sign up button click', () => {
      const formButtons = de.queryAll(By.css('form button[type="button"]'));
      const button = formButtons[formButtons.length - 1];
      spyOn(component, 'signUp');

      button.triggerEventHandler('click', null);

      expect(component.signUp).toHaveBeenCalled();
    });
  });
});
