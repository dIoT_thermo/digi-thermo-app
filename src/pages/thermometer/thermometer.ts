import { Component } from '@angular/core';

import {
  IonicPage,
  NavController,
  NavParams,
  AlertController,
  LoadingController,
  AlertOptions,
  Loading,
  Alert
} from 'ionic-angular';

import { Store } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/share';

import { BasePage } from './../base.page';
import * as fromRoot from './../../store/app.reducer';
import * as navigation from './../../store/navigation/navigation.actions';
import * as sensor from '../../store/sensor/sensor.actions';
import * as profile from '../../store/profile/profile.actions';

import { IDeviceReading } from './../../store/sensor/sensor.types';
import { IDataLog } from '../../store/profile/profile.types';

@IonicPage()
@Component({
  selector: 'page-thermometer',
  templateUrl: 'thermometer.html',
})

export class ThermometerPage extends BasePage {
  saveAlert: Alert;

  public connected$: Observable<boolean>;
  public lastReading$: Observable<IDeviceReading>;
  public temperatureUnit$: Observable<string>;

  public alert: Alert;

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    protected store: Store<fromRoot.State>,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
  ) {
    super(store);

    this.connected$ = store.select(fromRoot.getSensorIsConnected);

    this.lastReading$ = this.connected$
      .combineLatest(store.select(fromRoot.getSensorLastReading))
      .map(([isConnected, lastReading]) => isConnected ? lastReading : null)
      .filter(reading => reading !== null)
      .share();

    this.temperatureUnit$ = store.select(fromRoot.getProfileTemperatureUnit);
  }

  ionViewDidEnter() {
    this.connected$
      .filter(isConnected => !!isConnected)
      .take(1)
      .subscribe(() => {
        this.store.dispatch(new sensor.GetSensorDataAction());
        // This ensures the sensor is emitting
        this.store.dispatch(new sensor.SetEmittingStateAction(1));
      });
  }

  ionViewWillLeave() {
    // This disables the sensor emitting
    this.store.dispatch(new sensor.SetEmittingStateAction(0));

    if (this.alert) { this.alert.dismiss(); }
  }

  public openFeedbackModal() {
    this.store.dispatch(new navigation.OpenModalAction('FeedbackModal'));
  }

  public openHelpPage() {
    this.store.dispatch(new navigation.OpenBrowserAction('http://www.digithermo.com/help-page/'));
  }

  public saveReading(reading: IDeviceReading) {
    const options: AlertOptions = {
      title: 'Save Measurement',
      cssClass: 'save',
      inputs: [
        {
          type: 'text',
          name: 'title',
          placeholder: 'Title',
        },
        {
          type: 'text',
          name: 'comment',
          placeholder: 'Comment',
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Save',
          handler: (popupData) => {
            this.createDataLogItem(reading, popupData);
          }
        },
      ],
      enableBackdropDismiss: true,
    };
    this.saveAlert = this.alertCtrl.create(options);

    this.saveAlert.present();
  }

  public createDataLogItem(reading: IDeviceReading, info: IDataLog) {
    const payload: IDataLog = {
      title: info.title,
      comment: info.comment,
      temperature: reading.temperature,
      humidity: reading.humidity,
    };

    this.store.dispatch(new profile.CreateDataLogAction(payload));
  }
}
