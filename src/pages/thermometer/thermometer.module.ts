import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ThermometerPage } from './thermometer';
import { SharedModule } from './../../shared/shared.module';

@NgModule({
  declarations: [
    ThermometerPage,
  ],
  imports: [
    IonicPageModule.forChild(ThermometerPage),
    SharedModule,
  ],
  exports: [
    ThermometerPage
  ]
})
export class ThermometerModule {}
