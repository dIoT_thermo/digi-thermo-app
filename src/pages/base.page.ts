import { ViewChild } from '@angular/core';

import { Navbar } from 'ionic-angular';

import { Store } from '@ngrx/store';

import * as fromRoot from '../store/app.reducer';
import * as navigation from '../store/navigation/navigation.actions';

export class BasePage {
  @ViewChild(Navbar)
  _navbar: Navbar;

  constructor(
    protected store: Store<fromRoot.State>
  ) {}

  ionViewDidLoad() {
    if (typeof this._navbar !== 'undefined') {
      this._navbar.backButtonClick = (e: UIEvent) => {
        this.store.dispatch(new navigation.PopAction());
      };
    }
  }
}
