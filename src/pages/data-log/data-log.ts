import { Component } from '@angular/core';

import { IonicPage, AlertController, AlertOptions, Alert, Loading, LoadingController } from 'ionic-angular';

import { Store } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { BasePage } from './../base.page';
import * as fromRoot from '../../store/app.reducer';
import * as navigation from '../../store/navigation/navigation.actions';
import * as profile from '../../store/profile/profile.actions';
import { IDataLog, IDataLogUpdate } from './../../store/profile/profile.types';
import { IErrorAlert } from './../../models/error-alert';

@IonicPage()
@Component({
  selector: 'page-data-log',
  templateUrl: 'data-log.html',
})
export class DataLogPage extends BasePage {
  public dataLogs$: Observable<Array<IDataLog>>;
  public profileError$: Observable<IErrorAlert>;
  public isUpdating$: Observable<boolean>;
  public temperatureUnit$: Observable<string>;
  public subscriptions: Subscription = new Subscription();

  public loadingSpinner: Loading;
  public editAlert: Alert;
  public errorAlert: Alert;

  constructor(
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    protected store: Store<fromRoot.State>
  ) {
    super(store);
    this.dataLogs$ = store.select(fromRoot.getProfileDataLogs);
    this.profileError$ = store.select(fromRoot.getProfileError);
    this.isUpdating$ = store.select(fromRoot.getProfileIsUpdating);
    this.temperatureUnit$ = store.select(fromRoot.getProfileTemperatureUnit);
  }

  ionViewDidEnter() {
    const isRegisteringSub = this.isUpdating$
    .subscribe(this.handleUpdating.bind(this));

    const authErrorSub = this.profileError$
    .subscribe(this.handleUpdateError.bind(this));

    this.subscriptions.add(isRegisteringSub);
    this.subscriptions.add(authErrorSub);
    this.store.dispatch(new profile.GetDataLogsAction());
  }

  ionViewWillLeave() {
    this.subscriptions.unsubscribe();

    if (this.editAlert) { this.editAlert.dismiss(); }
    if (this.errorAlert) { this.errorAlert.dismiss(); }
  }

  public openFeedbackModal() {
    this.store.dispatch(new navigation.OpenModalAction('FeedbackModal'));
  }

  public openHelpPage() {
    this.store.dispatch(new navigation.OpenBrowserAction('http://www.digithermo.com/help-page/'));
  }

  public handleUpdating(isUpdating: boolean): void {
    if (!isUpdating && this.loadingSpinner) {
      this.loadingSpinner.dismiss();
    } else if (isUpdating && !this.loadingSpinner) {
      this.loadingSpinner = this.loadingCtrl.create();
      this.loadingSpinner.present();
    }
  }

  public editDataLogItem(dataLog: IDataLog) {
    const options: AlertOptions = {
      title: 'Edit Measurement',
      cssClass: 'edit',
      inputs: [
        {
          type: 'text',
          name: 'title',
          placeholder: 'Title',
          value: dataLog.title,
        },
        {
          type: 'text',
          name: 'comment',
          placeholder: 'Comment',
          value: dataLog.comment,
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Save',
          handler: this.updateDataLogItem.bind(this, dataLog),
        },
      ],
      enableBackdropDismiss: true,
    };
    this.editAlert = this.alertCtrl.create(options);

    this.editAlert.present();
  }

  public updateDataLogItem(dataLog: IDataLog, updates: { title?: string, comment?: string }) {
    const payload: IDataLogUpdate = { $key: dataLog.$key, updates };
    this.store.dispatch(new profile.UpdateDataLogAction(payload));
  }

  public handleUpdateError(error: IErrorAlert): void {
    if (!error) { return; }

    this.errorAlert = this.alertCtrl.create({
      title: error.title,
      message: error.message,
      buttons: error.buttons
    });

    this.errorAlert.onDidDismiss(() => {
      this.store.dispatch(new profile.DismissErrorAction());
    });

    this.errorAlert.present();
  }
}
