import { SharedModule } from './../../shared/shared.module';
import { NgModule } from '@angular/core';

import { IonicPageModule } from 'ionic-angular';

import { DataLogPage } from './data-log';
import { DataLogItemComponent } from './../../components/data-log-item/data-log-item';

@NgModule({
  declarations: [
    DataLogPage,
    DataLogItemComponent
  ],
  imports: [
    IonicPageModule.forChild(DataLogPage),
    SharedModule,
  ],
  exports: [
    DataLogPage,
    DataLogItemComponent
  ]
})
export class DataLogModule {}
