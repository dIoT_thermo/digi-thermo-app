import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement, NO_ERRORS_SCHEMA } from '@angular/core';

import { IonicModule, AlertController, LoadingController, Alert, Loading } from 'ionic-angular';

import { MomentModule } from 'angular2-moment';

import { StoreModule, Store } from '@ngrx/store';

import { Subscription } from 'rxjs/Subscription';

import { SharedModule } from './../../shared/shared.module';
import { DataLogPage } from './data-log';
import { DataLogItemComponent } from './../../components/data-log-item/data-log-item';
import { State as RootState, reducer, initialState } from './../../store/app.reducer';
import { AlertControllerMock } from './../../mocks/ionic/alert-controller.mock';
import { LoadingControllerMock } from './../../mocks/ionic/loading-controller.mock';

import { StoreMock } from './../../mocks/ngrx/store.mock';
import * as profile from './../../store/profile/profile.actions';
import * as navigation from './../../store/navigation/navigation.actions';
import * as fromRoot from './../../store/app.reducer';

describe('DataLogPage', () => {
  let _store: Store<RootState>;
  let fixture: ComponentFixture<DataLogPage>;
  let component: DataLogPage;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DataLogPage, DataLogItemComponent],
      imports: [
        IonicModule.forRoot(DataLogPage),
        StoreModule.provideStore(reducer),
        MomentModule,
        SharedModule,
      ],
      providers: [
        {
          provide: AlertController,
          useClass: AlertControllerMock,
        },
        {
          provide: LoadingController,
          useClass: LoadingControllerMock,
        },
        {
          provide: Store,
          useValue: new StoreMock(initialState),
        }
      ],
      // schemas: [ NO_ERRORS_SCHEMA ],
    });
  }));

  beforeEach(() => {
    _store = TestBed.get(Store);
    fixture = TestBed.createComponent(DataLogPage);
    component = fixture.componentInstance;
    de = fixture.debugElement;
  });

  it('should be created', () => {
    expect(component instanceof DataLogPage).toBeTruthy();
  });

  describe('Properties', () => {
    describe('dataLogs$', () => {
      it('should be created by selecting dataLogs from profile state', () => {
        const expected = _store.select(fromRoot.getProfileDataLogs);

        expect(component.dataLogs$).toEqual(expected);
      });
    });

    describe('isUpdating$', () => {
      it('should be created by selecting isUpdating from profile state', () => {
        const expected = _store.select(fromRoot.getProfileIsUpdating);

        expect(component.isUpdating$).toEqual(expected);
      });
    });

    describe('profileError$', () => {
      it('should be created by selecting error from profile state', () => {
        const expected = _store.select(fromRoot.getProfileError);

        expect(component.profileError$).toEqual(expected);
      });
    });

    describe('subscriptions', () => {
      it('should be created', () => {
        expect(component.subscriptions instanceof Subscription).toBeTruthy();
      });
    });

    describe('loadingSpinner', () => {
      it('should be undefined by default', () => {
        expect(component.loadingSpinner).toBeUndefined();
      });
    });

    describe('editAlert', () => {
      it('should be undefined by default', () => {
        expect(component.editAlert).toBeUndefined();
      });
    });

    describe('errorAlert', () => {
      it('should be undefined by default', () => {
        expect(component.errorAlert).toBeUndefined();
      });
    });
  });

  describe('Methods', () => {
    const $key = 'fakeKey';
    const fakeDataLog = { $key, humidity: 50, temperature: 50 };

    describe('ionViewDidEnter()', () => {
      it('should subscribe to isUpdating$', () => {
        spyOn(component.isUpdating$, 'subscribe');

        component.ionViewDidEnter();

        expect(component.isUpdating$.subscribe).toHaveBeenCalled();
      });

      it('should subscribe to profileError$', () => {
        spyOn(component.profileError$, 'subscribe');

        component.ionViewDidEnter();

        expect(component.profileError$.subscribe).toHaveBeenCalled();
      });

      it('should add two subscriptions in total', () => {
        component.ionViewDidEnter();

        expect(component.subscriptions['_subscriptions'].length).toBe(2);
      });

      it('should dispatch GetDataLogsAction', () => {
        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof profile.GetDataLogsAction).toBeTruthy();
          expect(action.type).toBe(profile.GET_DATA_LOGS);
        });

        component.ionViewDidEnter();
      });
    });

    describe('ionViewWillLeave()', () => {
      it('should unsubscribe from observables', () => {
        spyOn(component.subscriptions, 'unsubscribe');

        component.ionViewWillLeave();

        expect(component.subscriptions.unsubscribe).toHaveBeenCalled();
      });

      it('should dismiss any present alerts', () => {
        component.editAlert = <Alert>{dismiss() {}};
        component.errorAlert = <Alert>{dismiss() {}};
        spyOn(component.editAlert, 'dismiss');
        spyOn(component.errorAlert, 'dismiss');

        component.ionViewWillLeave();

        expect(component.editAlert.dismiss).toHaveBeenCalled();
        expect(component.errorAlert.dismiss).toHaveBeenCalled();
      });
    });

    describe(('openFeedbackModal()'), () => {
      it('should dispatch OpenModalAction with FeedbackModal as payload', () => {
        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof navigation.OpenModalAction).toBeTruthy();
          expect(action.type).toBe(navigation.OPEN_MODAL);
          expect(action.payload).toBe('FeedbackModal');
        });

        component.openFeedbackModal();
      });
    });

    describe(('openHelpPage()'), () => {
      it('should dispatch a new navigation.OpenBrowserAction', () =>  {
          const fakeURL = 'http://www.digithermo.com/help-page/';

          spyOn(_store, 'dispatch').and.callFake(action => {
            expect(action instanceof navigation.OpenBrowserAction).toBeTruthy();
            expect(action.type).toBe(navigation.OPEN_BROWSER);
            expect(action.payload).toEqual(fakeURL);
          });

          component.openHelpPage();
      });
    });

    describe(('editDataLogItem()'), () => {
      it('should present an alert when clicked', () => {
        spyOn(component.alertCtrl, 'create').and.callThrough();

        component.editDataLogItem(fakeDataLog);

        expect(component.alertCtrl.create).toHaveBeenCalled();
      });
    });

    describe('handleUpdating()', () => {
      it('should dismiss any present spinner when state isUpdating is false', () => {
        component.loadingSpinner = <Loading>{ dismiss() {} };
        spyOn(component.loadingSpinner, 'dismiss');

        component.handleUpdating(false);

        expect(component.loadingSpinner.dismiss).toHaveBeenCalled();
      });

      it('should create a loading spinner when state isUpdating is true', () => {
        spyOn(component.loadingCtrl, 'create').and.callThrough();

        component.handleUpdating(true);

        expect(component.loadingCtrl.create).toHaveBeenCalled();
      });
    });

    describe('udateDataLogItem()', () => {
      it('should dispatch UpdateDataLogAction', () => {
        const updates = { title: 'fake' };
        const fakeUpdate = { $key, updates };
        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof profile.UpdateDataLogAction).toBeTruthy();
          expect(action.type).toBe(profile.UPDATE_DATA_LOG);
          expect(action.payload).toEqual(fakeUpdate);
        });

        component.updateDataLogItem(fakeDataLog, updates);
      });
    });

    describe('handleUpdateError()', () => {
      it('should create an alert when passed an error', () => {
        const fakeError = {
          title: 'Alert!',
          message: 'Random Message',
          buttons: ['Fake', 'Buttons']
        };
        spyOn(component.alertCtrl, 'create').and.callThrough();

        component.handleUpdateError(fakeError);

        expect(component.alertCtrl.create).toHaveBeenCalled();
      });
    });
  });

  describe('Template Integration', () => {
    beforeEach(() => {
      fixture.detectChanges();
    });

    it('should call openFeedbackModal() on feedback button click', () => {
      const button = de.query(By.css('ion-buttons button.feedback'));
      spyOn(component, 'openFeedbackModal');

      button.triggerEventHandler('click', null);

      expect(component.openFeedbackModal).toHaveBeenCalled();
    });

    it('should call openHelpPage() on help button click', () => {
      const button = de.query(By.css('ion-buttons button[icon-only]'));
      spyOn(component, 'openHelpPage');

      button.triggerEventHandler('click', null);

      expect(component.openHelpPage).toHaveBeenCalled();
    });

    it('should call editDataLogItem() on data log item click', () => {
      fixture.whenStable().then(() => {
        const el = de.query(By.css('ion-item'));
      });
    });
  });
});
