import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { IonicModule } from 'ionic-angular';
import { StoreModule, Store } from '@ngrx/store';

import { SettingsPage } from './settings';
import { State as RootState, reducer, initialState } from './../../store/app.reducer';

import { StoreMock } from './../../mocks/ngrx/store.mock';
import * as profile from './../../store/profile/profile.actions';
import * as navigation from './../../store/navigation/navigation.actions';
import * as sensor from './../../store/sensor/sensor.actions';
import * as fromRoot from './../../store/app.reducer';

describe('Settings', function() {
  let _store: Store<RootState>;
  let fixture: ComponentFixture<SettingsPage>;
  let comp: SettingsPage;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsPage ],
      imports: [
        IonicModule.forRoot(SettingsPage),
        StoreModule.provideStore(reducer)
      ],
      providers: [
        { provide: Store,
          useValue: new StoreMock(initialState)
        },
      ]
    });
  }));

  beforeEach(() => {
    _store = TestBed.get(Store);
    fixture = TestBed.createComponent(SettingsPage);
    comp = fixture.componentInstance;
    de = fixture.debugElement;
  });

  it('Should create Settings component', () => {
    expect(comp instanceof SettingsPage).toBeTruthy();
  });

  describe('Methods', () => {
    describe('Modals', () => {
      it('openFeedbackModal() should dispatch a new navigation.OpenModalAction', () => {
        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof navigation.OpenModalAction).toBeTruthy();
          expect(action.type).toBe(navigation.OPEN_MODAL);
        });

        comp.openFeedbackModal();
      });

      it('openFeedbackModal() should NOT dispatch any other action besides navigation.OpenModalAction', () => {
        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof navigation.PushAction).toBeFalsy();
          expect(action.type).not.toBe(navigation.PUSH);
        });

        comp.openFeedbackModal();
      });
    });

    describe('openHelpPage()', () => {
      it('should dispatch a new navigation.OpenBrowserAction', () =>  {
          const fakeURL = 'http://www.digithermo.com/help-page/';

          spyOn(_store, 'dispatch').and.callFake(action => {
            expect(action instanceof navigation.OpenBrowserAction).toBeTruthy();
            expect(action.type).toBe(navigation.OPEN_BROWSER);
            expect(action.payload).toEqual(fakeURL);
          });

          comp.openHelpPage();
      });
    });

    describe('changeUnit()', () => {
      it('should dispatch a new profile.UpdateTemperatureUnitAction', () => {
        const newUnit = 'C';
        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof profile.UpdateTemperatureUnitAction).toBeTruthy();
          expect(action.type).toBe(profile.UPDATE_TEMPERATURE_UNIT);
          expect(action.payload).toEqual(newUnit);
        });

        comp.changeUnit(newUnit);
      });
    });

    describe('logout()', () => {
      it('should dispatch an action to disconnect from the sensor', () => {
        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof sensor.DisconnectAction).toBeTruthy();
          expect(action.type).toBe(sensor.DISCONNECT);
          expect(action.payload).toBeUndefined();
        });

        comp.logout();
      });
    });
  });
});
