import { Component } from '@angular/core';

import { IonicPage } from 'ionic-angular';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';

import { BasePage } from './../base.page';
import * as fromRoot from '../../store/app.reducer';
import * as navigation from '../../store/navigation/navigation.actions';
import * as profile from '../../store/profile/profile.actions';
import * as sensor from '../../store/sensor/sensor.actions';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage extends BasePage {
  public tempUnit$: Observable<any>;

  constructor(
    protected store: Store<fromRoot.State>
  ) {
    super(store);
    this.tempUnit$ = this.store.select(fromRoot.getProfileTemperatureUnit);
  }

  public openFeedbackModal() {
    this.store.dispatch(new navigation.OpenModalAction('FeedbackModal'));
  }

  public openHelpPage() {
    this.store.dispatch(new navigation.OpenBrowserAction('http://www.digithermo.com/help-page/'));
  }

  public logout() {
    this.store.dispatch(new sensor.DisconnectAction());
  }

  public changeUnit(newUnit: string) {
    this.store.dispatch(new profile.UpdateTemperatureUnitAction(newUnit));
  }

}
