import {} from 'jasmine';
import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';

import { IonicModule, AlertController, LoadingController, Alert, Loading } from 'ionic-angular';
import { StoreModule, Store } from '@ngrx/store';

import { Subscription } from 'rxjs/Subscription';

import { RegisterPage } from '../register/register';
import { State as RootState, reducer, initialState } from './../../store/app.reducer';
import { AlertControllerMock } from './../../mocks/ionic/alert-controller.mock';
import { LoadingControllerMock } from './../../mocks/ionic/loading-controller.mock';

import { StoreMock } from './../../mocks/ngrx/store.mock';
import * as authentication from './../../store/authentication/authentication.actions';
import * as navigation from './../../store/navigation/navigation.actions';
import * as fromRoot from './../../store/app.reducer';
import { AuthService } from './../../providers/auth.service';
import { AuthServiceMock } from './../../mocks/auth.service.mock';

describe('Registration', function() {
  let _store: Store<RootState>;
  let fixture: ComponentFixture<RegisterPage>;
  let de: DebugElement;
  let comp: RegisterPage;

  beforeEach(async(() => {
    TestBed.configureTestingModule({

      declarations: [ RegisterPage ],
      imports: [
        IonicModule.forRoot(RegisterPage),
        StoreModule.provideStore(reducer)
      ],
      providers: [
        FormBuilder,
        {
          provide: AlertController,
          useClass: AlertControllerMock,
        },
        {
          provide: LoadingController,
          useClass: LoadingControllerMock,
        },
        { provide: AuthService,
          useClass: AuthServiceMock
        },
        { provide: Store,
          useValue: new StoreMock(initialState)
        }
      ]
    });
  }));

  beforeEach(() => {
    _store = TestBed.get(Store);
    fixture = TestBed.createComponent(RegisterPage);
    comp = fixture.componentInstance;
    de = fixture.debugElement;
  });

  it('Should create Register component', () => {
    expect(comp instanceof RegisterPage).toBeTruthy();
  });

  describe('Properties', () => {
    describe('logo', () => {
      it('should be an image', () => {
        de = fixture.debugElement.query(By.css('#logo'));
      });
    });

    describe('headingText', () => {
      it('should be an h2 element', () => {
        de = fixture.debugElement.query(By.css('#heading'));
      });
    });

    describe('Register form', () => {
      it('should have three input fields', () => {
        const controls = Object.keys(comp.registerForm.controls);

        expect(controls.length).toBe(3);
      });
    });

    describe('registerForm', () => {
      describe('Email Input', () => {
        let formControl: FormControl;
        beforeEach(() => {
          formControl = comp.registerForm.controls['email'] as FormControl;
        });

        it('should exist', () => {
          expect(formControl).toBeDefined();
        });

        it('should be required', () => {
          const errors = formControl.errors || {};
          expect(errors['required']).toBeTruthy();
        });
      });

      describe('Password Input', () => {
        let formControl: FormControl;
        beforeEach(() => {
          formControl = comp.registerForm.controls['password'] as FormControl;
        });

        it('should exist', () => {
          expect(formControl).toBeDefined();
        });

        it('should be required', () => {
          const errors = formControl.errors || {};

          expect(errors['required']).toBeTruthy();
        });
      });

      describe('Confirm Password Input', () => {
        let formControl: FormControl;
        beforeEach(() => {
          formControl = comp.registerForm.controls['confirmPassword'] as FormControl;
        });

        it('Should exist', () => {
          expect(formControl).toBeDefined();
        });

        it('should be required', () => {
          const errors = formControl.errors || {};

          expect(errors['required']).toBeTruthy();
        });
      });

      describe('Submit button', () => {
        beforeEach(() => {
          de = fixture.debugElement.query(By.css('button[type="submit"]'));
        });

        it('should exist', () => {
          expect(de).toBeDefined();
        });

        it('should dispatch a new authentication.RegisterAction', () => {
          const fakeCredentials = {
            email: 'fake@email.com',
            password: 'test123',
            confirmPassword: '123test'
          };
          expect(comp.registerForm.valid).toBeFalsy();

          comp.registerForm.controls['email'].setValue(fakeCredentials.email);
          comp.registerForm.controls['password'].setValue(fakeCredentials.password);
          comp.registerForm.controls['confirmPassword'].setValue(fakeCredentials.confirmPassword);
          expect(comp.registerForm.valid).toBeTruthy();
          spyOn(_store, 'dispatch').and.callFake((action) => {
            expect(action instanceof authentication.RegisterAction).toBeTruthy();
            expect(action.type).toBe(authentication.REGISTER);
            expect(action.payload).toEqual(fakeCredentials);
          });

          comp.register();
        });
      });

      describe('disableRegisterButton$', () => {
        it('should be updated on register form status changes', () => {
          const emailInput = comp.registerForm.controls['email'];
          const passwordInput = comp.registerForm.controls['password'];
          const confirmPasswordInput = comp.registerForm.controls['confirmPassword'];

          let testCase = 0;
          comp.disableRegisterButton$.subscribe((disable) => {
            // Initial value
            if (testCase === 0) {
              expect(disable).toBeTruthy();
            }

            // Invalid Form
            if (testCase === 1) {
              expect(disable).toBeTruthy();
            }

            // Valid Form
            if (testCase === 2) {
              expect(disable).toBeTruthy();
            }

            if (testCase === 3) {
              expect(disable).toBeFalsy();
            }
          });

          // Invalid Form
          testCase++;
          emailInput.setValue('valid@email.com');

          // Valid Form
          testCase++;
          passwordInput.setValue('validPassword');

          testCase++;
          confirmPasswordInput.setValue('validPassword');
        });
      });

      describe('isLoading$', () => {
        it('should be created by selecting isRegistering from authentication state', () => {
          const expected = _store.select(fromRoot.getAuthIsRegistering);

          expect(comp.isRegistering$).toEqual(expected);
        });
      });

      describe('authError$', () => {
        it('should be created by selecting error from authentication state', () => {
          const expected = _store.select(fromRoot.getAuthError);

          expect(comp.authError$).toEqual(expected);
        });
      });

      describe('loadingSpinner', () => {
        it('should be undefined by default', () => {
          expect(comp.loadingSpinner).toBeUndefined();
        });
      });

      describe('alert', () => {
        it('should be undefined by default', () => {
          expect(comp.alert).toBeUndefined();
        });
      });
    });

    describe('Methods', () => {
      describe('ionViewWillEnter()', () => {
        it('should subscribe to isRegistering$', () => {
          spyOn(comp.isRegistering$, 'subscribe');

          comp.ionViewWillEnter();

          expect(comp.isRegistering$.subscribe).toHaveBeenCalled();
        });

        it('should subscribe to authError$', () => {
          spyOn(comp.authError$, 'subscribe');

          comp.ionViewWillEnter();

          expect(comp.authError$.subscribe).toHaveBeenCalled();
        });

        it('should add two subscriptions in total', () => {
          comp.ionViewWillEnter();

          expect(comp.subscriptions instanceof Subscription).toBeTruthy();
          expect(comp.subscriptions['_subscriptions'].length).toBe(2);
        });
      });

      describe('ionViewWillLeave()', () => {
        beforeEach(() => {
          comp.ionViewWillEnter();
        });

        it('should unsubscribe from observables', () => {
          spyOn(comp.subscriptions, 'unsubscribe');

          comp.ionViewWillLeave();

          expect(comp.subscriptions.unsubscribe).toHaveBeenCalled();
        });

        it('should dismiss any present alerts', () => {
          comp.alert = <Alert>{dismiss() {}};
          spyOn(comp.alert, 'dismiss');

          comp.ionViewWillLeave();

          expect(comp.alert.dismiss).toHaveBeenCalled();
        });
      });

      describe('register()', () => {
        it('should dispatch a new authentication.RegisterAction', () => {
          const fakeCredentials = {
            email: 'fake@email.com',
            password: '123test',
            confirmPassword: '123test'
          };
          comp.registerForm.setValue(fakeCredentials);

          spyOn(_store, 'dispatch').and.callFake((action) => {
            expect(action instanceof authentication.RegisterAction).toBeTruthy();
            expect(action.type).toBe(authentication.REGISTER);
            expect(action.payload).toEqual(fakeCredentials);
          });

          comp.register();
        });
      });
      describe('viewTerms()', () => {
        it('should dispatch a new navigation.OpenBrowserAction', () =>  {
          const fakeURL = 'http://www.digithermo.com/privacy-policy/';

          spyOn(_store, 'dispatch').and.callFake(action => {
            expect(action instanceof navigation.OpenBrowserAction).toBeTruthy();
            expect(action.type).toBe(navigation.OPEN_BROWSER);
            expect(action.payload).toEqual(fakeURL);
          });

          comp.viewTerms();
        });
      });
    });
  });
});
