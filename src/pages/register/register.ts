import { Component } from '@angular/core';

import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';

import {
  IonicPage,
  Alert,
  AlertController,
  LoadingController,
  Loading
} from 'ionic-angular';


import { Store } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/startWith';

import * as fromRoot from '../../store/app.reducer';
import * as navigation from '../../store/navigation/navigation.actions';
import * as authentication from '../../store/authentication/authentication.actions';
import { ILoginCredentials } from '../../store/authentication/authentication.types';
import { BasePage } from './../base.page';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage extends BasePage {
  public registerForm: FormGroup;
  public disableRegisterButton$: Observable<boolean>;
  public subscriptions: Subscription;

  public isRegistering$: Observable<boolean>;
  public authError$: Observable<any>;

  public loadingSpinner: Loading;
  public alert: Alert;

  constructor(
    formBuilder: FormBuilder,
    protected store: Store<fromRoot.State>,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
  ) {
    super(store);

    this.registerForm = formBuilder.group({
      email: [ '', Validators.required],
      password: [ '', Validators.required],
      confirmPassword: [ '', Validators.required]
    });

    this.disableRegisterButton$ = this.registerForm.statusChanges
    .map(status => status !== 'VALID')
    .startWith(true);

    this.isRegistering$ = store.select(fromRoot.getAuthIsRegistering);
    this.authError$ = store.select(fromRoot.getAuthError);
  }

  ionViewWillEnter() {
    this.subscriptions = new Subscription();
    const isRegisteringSub = this.isRegistering$
    .subscribe(this.handleRegistering.bind(this));

    const authErrorSub = this.authError$
    .subscribe(this.handleAuthError.bind(this));

    this.subscriptions.add(isRegisteringSub);
    this.subscriptions.add(authErrorSub);
  }

  ionViewWillLeave() {
    this.subscriptions.unsubscribe();

    if (this.alert) { this.alert.dismiss(); }
  }

  public register(): void {
    const credentials: ILoginCredentials = this.registerForm.value;
    this.store.dispatch(new authentication.RegisterAction(credentials));
  }

  public viewTerms(): void {
    this.store.dispatch(new navigation.OpenBrowserAction('http://www.digithermo.com/privacy-policy/'));
  }

  public goBack(): void {
    this.store.dispatch(new navigation.PopAction());
  }

  public handleRegistering(isRegistering: boolean): void {
    if (!isRegistering && this.loadingSpinner) {
      this.loadingSpinner.dismiss();
    } else if (isRegistering && !this.loadingSpinner) {
      this.loadingSpinner = this.loadingCtrl.create();
      this.loadingSpinner.present();
    }
  }

  public handleAuthError(error: any): void {
    if (!error) { return; }

    this.alert = this.alertCtrl.create({
      title: error.title,
      message: error.message,
      buttons: error.buttons
    });

    this.alert.onDidDismiss(() => {
      this.store.dispatch(new authentication.DismissErrorAction());
    });

    this.alert.present();
  }
}
