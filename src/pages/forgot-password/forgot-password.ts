import { Component } from '@angular/core';

import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';

import {
  IonicPage,
  Alert,
  AlertController,
  LoadingController,
  Loading,
} from 'ionic-angular';

import { Store } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/startWith';

import { BasePage } from './../base.page';
import * as fromRoot from '../../store/app.reducer';
import * as navigation from '../../store/navigation/navigation.actions';
import { DismissErrorAction } from '../../store/authentication/authentication.actions';
import { ResetPasswordAction } from '../../store/authentication/authentication.actions';
import { ILoginCredentials } from '../../store/authentication/authentication.types';

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage extends BasePage {
  public resetPasswordForm: FormGroup;
  public disableResetPasswordButton$: Observable<boolean>;
  public subscriptions: Subscription;

  public isResettingPassword$: Observable<boolean>;
  public authError$: Observable<any>;

  public loadingSpinner: Loading;
  public alert: Alert;

  constructor(
    formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    protected store: Store<fromRoot.State>
  ) {
    super(store);

    this.resetPasswordForm = formBuilder.group({
      email: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])]
    });

    this.disableResetPasswordButton$ = this.resetPasswordForm.statusChanges
    .map(status => status !== 'VALID')
    .startWith(true);

    this.isResettingPassword$ = store.select(fromRoot.getAuthIsResettingPassword);
    this.authError$ = store.select(fromRoot.getAuthError);
  }

  ionViewWillEnter() {
    this.subscriptions = new Subscription();
    const isResettingPasswordSub = this.isResettingPassword$
      .subscribe(this.handleResettingPassword.bind(this));

    const authErrorSub = this.authError$
      .subscribe(this.handleAuthError.bind(this));

    this.subscriptions.add(isResettingPasswordSub);
    this.subscriptions.add(authErrorSub);
  }

  ionViewWillLeave() {
    this.subscriptions.unsubscribe();

    if (this.alert) { this.alert.dismiss(); }
  }

  public goBack(): void {
    this.store.dispatch(new navigation.PopAction());
  }

  public resetPassword(): void {
    const email = this.resetPasswordForm.value.email;
    this.store.dispatch(new ResetPasswordAction(email));
  }

  public handleResettingPassword(isResettingPassword: boolean): void {
    if (!isResettingPassword && this.loadingSpinner) {
      this.loadingSpinner.dismiss();
    } else if (isResettingPassword && !this.loadingSpinner) {
      this.loadingSpinner = this.loadingCtrl.create();
      this.loadingSpinner.present();
    }
  }

  public handleAuthError(error: any): void {
    if (!error) { return; }

    this.alert = this.alertCtrl.create({
      title: error.title,
      message: error.message,
      buttons: error.buttons
    });

    this.alert.onDidDismiss(() => {
      this.store.dispatch(new DismissErrorAction());
    });

    this.alert.present();
  }
}
