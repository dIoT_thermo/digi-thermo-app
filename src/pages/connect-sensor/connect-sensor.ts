import { Component } from '@angular/core';

import { IonicPage } from 'ionic-angular';

import { Store } from '@ngrx/store';

import { Observable } from 'rxjs/Observable';

import { BasePage } from './../base.page';
import * as fromRoot from '../../store/app.reducer';
import * as sensor from '../../store/sensor/sensor.actions';
import { IDevice } from '../../store/sensor/sensor.types';

@IonicPage()
@Component({
  selector: 'page-connect-sensor',
  templateUrl: 'connect-sensor.html',
})
export class ConnectSensorPage extends BasePage {
  public isConnecting$: Observable<boolean>;
  public deviceList$: Observable<IDevice[]>;

  constructor(
    protected store: Store<fromRoot.State>
  ) {
    super(store);

    this.isConnecting$ = store.select(fromRoot.getSensorIsConnecting);
    this.deviceList$ = store.select(fromRoot.getSensorDeviceList);
  }

  ionViewWillEnter() {
    this.store.dispatch(new sensor.IsBluetoothEnabledAction());
  }

  public connectSensor(selected) {
    this.store.dispatch(new sensor.ConnectAction(selected));
  }
}
