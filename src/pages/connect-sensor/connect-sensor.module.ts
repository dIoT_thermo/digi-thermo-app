import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConnectSensorPage } from './connect-sensor';

@NgModule({
  declarations: [
    ConnectSensorPage,
  ],
  imports: [
    IonicPageModule.forChild(ConnectSensorPage),
  ],
  exports: [
    ConnectSensorPage
  ]
})
export class ConnectSensorModule {}
