import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';

import { IonicModule } from 'ionic-angular';
import { StoreModule, Store } from '@ngrx/store';

import { TabsPage } from './tabs';
import { State as RootState, reducer, initialState } from './../../store/app.reducer';

import { StoreMock } from './../../mocks/ngrx/store.mock';
import * as navigation from './../../store/navigation/navigation.actions';
import * as fromRoot from './../../store/app.reducer';

describe('Tabs Page', function() {
  let _store: Store<RootState>;
  let fixture: ComponentFixture<TabsPage>;
  let comp: TabsPage;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabsPage ],
      imports: [
        IonicModule.forRoot(TabsPage),
        StoreModule.provideStore(reducer)
      ],
      providers: [
        {
          provide: Store,
          useValue: new StoreMock(initialState)
        },
      ]
    });
  }));

  beforeEach(() => {
    _store = TestBed.get(Store);
    fixture = TestBed.createComponent(TabsPage);
    comp = fixture.componentInstance;
    de = fixture.debugElement;
  });

  it('Should create Settings component', () => {
    expect(comp instanceof TabsPage).toBeTruthy();
  });

  describe('Methods', () => {
    describe('onTabChange()', () => {
      it('should only set first load to true after being called', () => {
        const tab: any = { index: 1 };
        spyOn(_store, 'dispatch');

        comp.onTabChange(tab);

        expect(_store.dispatch).not.toHaveBeenCalled();
        expect(comp.isFirstLoad).toBeFalsy();
      });

      it('should dispatch a new navigation.ChangeStackAction if it wasnt the first load', () => {
        const tab: any = { index: 1 };
        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof navigation.ChangeStackAction).toBeTruthy();
          expect(action.type).toBe(navigation.CHANGE_STACK);
        });

        // first one shouldnt trigger dispatch
        comp.onTabChange(tab);
        expect(_store.dispatch).not.toHaveBeenCalled();

        // second one should trigger it
        comp.onTabChange(tab);
        expect(_store.dispatch).toHaveBeenCalled();

        expect(comp.isFirstLoad).toBeFalsy();
      });
    });
  });
});
