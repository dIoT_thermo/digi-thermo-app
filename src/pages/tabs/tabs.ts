import { Component, ViewChild } from '@angular/core';

import { IonicPage, Tab, Tabs } from 'ionic-angular';

import { Store } from '@ngrx/store';

import { BasePage } from './../base.page';
import { TABS } from './../../store/navigation/navigation.types';
import * as fromRoot from './../../store/app.reducer';
import * as navigation from './../../store/navigation/navigation.actions';


@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
@IonicPage()
export class TabsPage extends BasePage {
  public tabs = TABS;
  public isFirstLoad = true;

  constructor(
    protected store: Store<fromRoot.State>
  ) {
    super(store);
  }

  ionViewDidLoad() {
    super.ionViewDidLoad();
  }

  public onTabChange(tab: Tab) {
    if (this.isFirstLoad) {
      this.isFirstLoad = false;
      return;
    }

    const stack = TABS[tab.index].stack;
    this.store.dispatch(new navigation.ChangeStackAction(stack));
  }
}
