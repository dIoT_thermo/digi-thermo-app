interface IEnvironmentConfig {
  firebaseConfig: { [key: string]: string };
}

const environment: { production: boolean } = { production: false };

const DevEnvironmentConfig: IEnvironmentConfig = {
  firebaseConfig: {
    apiKey: 'AIzaSyB4at38akmpDoEgmZi-XrwD68oGrWSx3G8',
    authDomain: 'digi-thermo-b459f.firebaseapp.com',
    databaseURL: 'https://digi-thermo-b459f.firebaseio.com',
    projectId: 'digi-thermo-b459f',
    storageBucket: 'digi-thermo-b459f.appspot.com',
    messagingSenderId: '110354954103'
  }
};

export { environment, DevEnvironmentConfig };
