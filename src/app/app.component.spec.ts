import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { IonicModule, Platform } from 'ionic-angular';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Geolocation } from '@ionic-native/geolocation';

import { StoreModule, Store } from '@ngrx/store';

import { State as RootState, reducer, initialState } from './../store/app.reducer';
import * as authentication from './../store/authentication/authentication.actions';
import { AppComponent } from './app.component';
import { PlatformMock } from './../mocks/ionic/platform.mock';
import { StoreMock } from './../mocks/ngrx/store.mock';
import { GeolocationMock } from './../mocks/ionic-native/geolocation.mock';

describe('AppComponent', () => {
  let _store: Store<RootState>;
  let fixture: ComponentFixture<AppComponent>;
  let component: AppComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [
        IonicModule.forRoot(AppComponent),
        StoreModule.provideStore(reducer)
      ],
      providers: [
        StatusBar,
        SplashScreen,
        {
          provide: Platform,
          useClass: PlatformMock,
        },
        {
          provide: Store,
          useValue: new StoreMock(initialState),
        },
        {
          provide: Geolocation,
          useClass: GeolocationMock,
        }
      ]
    });
  }));

  beforeEach(() => {
    _store = TestBed.get(Store);
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  });

  it('should be created', () => {
    expect(component instanceof AppComponent).toBe(true);
  });

  describe('Properties', () => {
    describe('rootPage', () => {
      it('should have \'LoginPage\' as rootPage', () => {
        expect(component.rootPage).toBe('LoginPage');
      });
    });
  });

  describe('Methods', () => {
    describe('OnInit', () => {
      it('should dispatch CheckAuthStateAction', () => {
        spyOn(_store, 'dispatch').and.callFake((action) => {
          expect(action instanceof authentication.CheckAuthStateAction).toBeTruthy();
          expect(action.type).toBe(authentication.CHECK_AUTH_STATE);
          expect(action.payload).toBeUndefined();
        });

        component.ngOnInit();
      });

      it('should set StatusBar style to light content', fakeAsync(() => {
        spyOn(component.statusBar, 'styleLightContent');

        component.ngOnInit();
        tick();

        expect(component.statusBar.styleLightContent).toHaveBeenCalled();
      }));

      it('should hide SplashScreen', fakeAsync(() => {
        spyOn(component.splashScreen, 'hide');

        component.ngOnInit();
        tick();

        expect(component.splashScreen.hide).toHaveBeenCalled();
      }));

      it('should attempt to get the device\'s current position', fakeAsync(() => {
        spyOn(component.geolocation, 'getCurrentPosition').and.returnValue(Promise.reject('fail'));

        component.ngOnInit();
        tick();

        expect(component.geolocation.getCurrentPosition).toHaveBeenCalled();
      }));

      it('should dispatch SetGeolocationAction on success', fakeAsync(() => {
        const fakeCoords = {
          latitude: GeolocationMock.fakeCoords.latitude,
          longitude: GeolocationMock.fakeCoords.longitude
        };
        let dispatchs = 0;
        spyOn(component.geolocation, 'getCurrentPosition').and.callThrough();
        spyOn(_store, 'dispatch').and.callFake((action) => {
          if (++dispatchs === 2) {
            expect(action instanceof authentication.SetGeolocationAction).toBeTruthy();
            expect(action.type).toBe(authentication.SET_GEOLOCATION);
            expect(action.payload).toEqual(fakeCoords);
          }
        });

        component.ngOnInit();
        tick();
      }));
    });
  });
});
