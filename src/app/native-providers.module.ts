import { InjectionToken, ModuleWithProviders, NgModule } from '@angular/core';

import { Facebook } from '@ionic-native/facebook';
import { Keyboard } from '@ionic-native/keyboard';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Diagnostic } from '@ionic-native/diagnostic';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Geolocation } from '@ionic-native/geolocation';
import { BLE } from '@ionic-native/ble';
import { BLEMock } from './../mocks/ionic-native/ble.mock';

const DOCUMENT_URL = new InjectionToken('document.url');
const DEFAULT_LIVERELOAD_URL = new RegExp(/^http:\/\/\d+\.\d+\.\d+\.\d+\:810[0-9]/);
const DEFAULT_WEBVIEW_URL = 'file:///';
const WK_WEBVIEW_URL = 'http://localhost:8080/var/containers/Bundle/Application';

export function documentUrlFactory() {
  return document.URL;
}

export function BLEFactory(docUrl: string) {
  if (docUrl.match(DEFAULT_LIVERELOAD_URL)
    || docUrl.startsWith(DEFAULT_WEBVIEW_URL)
    || docUrl.startsWith(WK_WEBVIEW_URL)
  ) {
    return new BLE();
  }

  return new BLEMock();
}

@NgModule({
  providers: [],
})
export class NativeProvidersModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: NativeProvidersModule,
      providers: [
        { provide: DOCUMENT_URL, useFactory: documentUrlFactory },
        Facebook,
        Keyboard,
        SplashScreen,
        StatusBar,
        Diagnostic,
        InAppBrowser,
        Geolocation,
        {
          provide: BLE,
          useFactory: BLEFactory,
          deps: [ DOCUMENT_URL ]
        },
      ],
    };
  }
}
