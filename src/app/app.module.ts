import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';

import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { SharedModule } from './../shared/shared.module';
import { FeedbackModalComponent } from './../components/feedback-modal/feedback-modal';
import { reducer, initialState } from '../store/app.reducer';
import { NavigationEffects } from './../store/navigation/navigation.effects';
import { AuthenticationEffects } from './../store/authentication/authentication.effects';
import { SensorEffects } from './../store/sensor/sensor.effects';
import { ProfileEffects } from './../store/profile/profile.effects';
import { AppComponent } from './app.component';
import { AuthService } from './../providers/auth.service';
import { ProfileService } from './../providers/profile.service';
import { ModalService } from './../providers/modal.service';
import { BluetoothService } from './../providers/bluetooth.service';
import { DevEnvironmentConfig as env } from './../environment';
import { IonicStorageModule } from '@ionic/storage';
import { NativeProvidersModule } from './native-providers.module';

@NgModule({
  declarations: [
    AppComponent,
    FeedbackModalComponent,
  ],
  imports: [
    BrowserModule,
    NativeProvidersModule.forRoot(),
    AngularFireModule.initializeApp(env.firebaseConfig),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    StoreModule.provideStore(reducer, initialState),
    StoreDevtoolsModule.instrumentOnlyWithExtension(),
    EffectsModule.run(NavigationEffects),
    EffectsModule.run(AuthenticationEffects),
    EffectsModule.run(SensorEffects),
    EffectsModule.run(ProfileEffects),
    IonicModule.forRoot(AppComponent),
    IonicStorageModule.forRoot(),
    SharedModule,
  ],
  bootstrap: [ IonicApp ],
  entryComponents: [
    AppComponent,
    FeedbackModalComponent,
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    AuthService,
    ProfileService,
    ModalService,
    BluetoothService,
  ],
})
export class AppModule {}
