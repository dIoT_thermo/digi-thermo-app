import { Pipe, PipeTransform } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromRoot from './../store/app.reducer';
import * as _ from 'lodash';

type TemperatureUnit = 'C' | 'F';

@Pipe({
  name: 'temperaturePreference',
  pure: false,
})
export class TemperaturePreferencePipe implements PipeTransform {
  constructor(
    private store: Store<fromRoot.State>
  ) { }

  transform(temp: number) {
    return this.store.select(fromRoot.getProfileTemperatureUnit)
      .map((unit: TemperatureUnit) => unit === 'F' ? ((temp * 9 / 5) + 32) : temp);
  }
}

@Pipe({
  name: 'sortBy',
})
export class SortByPipe implements PipeTransform {
  transform(data: Array<any>, args?: any, descending = false) {
    const sorted = _.sortBy(data, [args]);
    return descending
      ? _.reverse(sorted)
      : sorted;
  }
}

@Pipe({
  name: 'roundNumber',
})
export class RoundNumberPipe implements PipeTransform {
  transform(num: number) {
    return Math.round(num);
  }
}
