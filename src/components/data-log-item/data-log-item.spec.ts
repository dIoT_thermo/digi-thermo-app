import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement, EventEmitter } from '@angular/core';

import { IonicModule } from 'ionic-angular';
import { MomentModule } from 'angular2-moment';
import { SharedModule } from './../../shared/shared.module';

import { StoreModule, Store } from '@ngrx/store';
import { State as RootState, reducer, initialState } from './../../store/app.reducer';

import { DataLogItemComponent } from './data-log-item';
import { StoreMock } from './../../mocks/ngrx/store.mock';
import { IDataLog } from './../../store/profile/profile.types';

describe('DataLogItemComponent', () => {
  let fixture: ComponentFixture<DataLogItemComponent>;
  let component: DataLogItemComponent;
  let de: DebugElement;
  let expectedInput: IDataLog;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DataLogItemComponent],
      imports: [
        IonicModule.forRoot(DataLogItemComponent),
        StoreModule.provideStore(reducer),
        MomentModule,
        SharedModule,
      ],
      providers: [
        {
          provide: Store,
          useValue: new StoreMock(initialState),
        },
      ],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataLogItemComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;

    expectedInput = {
      $key: 'fake',
      comment: 'fake',
      createdAt: 1000000,
      humidity: 50,
      temperature: 80,
      title: 'fake',
    };
    component.dataLog = expectedInput;
  });

  it('should be created', () => {
    expect(component instanceof DataLogItemComponent).toBeTruthy();
  });

  describe('Properties', () => {
    describe('Inputs', () => {
      it('should receive dataLog as input', () => {
        fixture.detectChanges();
        expect(component.dataLog).toEqual(expectedInput);
      });
    });

    describe('Outputs', () => {
      it('should have onClick output', () => {
        fixture.detectChanges();
        expect(component.onClick instanceof EventEmitter).toBeTruthy();
      });
    });
  });

  describe('Template Integration', () => {
    let itemEl: DebugElement;
    let temperatureEl: DebugElement;

    beforeEach(() => {
      itemEl = de.query(By.css('ion-item'));
    });

    it('should emit the data log when the item is clicked', () => {
      spyOn(component.onClick, 'emit');

      fixture.detectChanges();
      itemEl.triggerEventHandler('click', null);

      expect(component.onClick.emit).toHaveBeenCalled();
    });

    it('should NOT have the blue class when temperature is < 21 C', () => {
      fixture.detectChanges();

      expect(itemEl.classes['blue']).toBe(false);
    });

    it('should have the blue class when temperature is < 21 C', () => {
      component.dataLog.temperature = 10;

      fixture.detectChanges();

      expect(itemEl.classes['blue']).toBe(true);
    });

    it('should NOT have the three-digi class when temeprature is less than 3 digits', () => {
      fixture.detectChanges();

      fixture.whenStable()
        .then(() => {
          temperatureEl = de.query(By.css('.temp'));
          expect(temperatureEl.classes['three-digi']).toBe(false);
        });
    });

    it('should have the three-digi class when temeprature is less than 3 digits', () => {
      component.dataLog.temperature = 100;

      fixture.detectChanges();
      fixture.whenStable()
        .then(() => {
          temperatureEl = de.query(By.css('.temp'));
          expect(temperatureEl.classes['three-digi']).toBe(true);
        });
    });
  });
});
