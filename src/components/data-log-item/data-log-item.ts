import { Component, Input, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import * as fromRoot from '../../store/app.reducer';
import { IDataLog } from './../../store/profile/profile.types';

@Component({
  selector: 'data-log-item',
  templateUrl: 'data-log-item.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DataLogItemComponent {
  @Input()
  public dataLog: IDataLog;

  @Input()
  public unit: 'C' | 'F';

  @Output()
  public onClick: EventEmitter<any> = new EventEmitter();
}
