import { Component } from '@angular/core';

import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from '@angular/forms';

import {
  ViewController,
  Alert,
  AlertController,
  LoadingController,
  Loading
} from 'ionic-angular';

import { Store } from '@ngrx/store';


import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/startWith';

import * as fromRoot from '../../store/app.reducer';
import * as profile from '../../store/profile/profile.actions';
import { IFeedbackRating } from '../../store/profile/profile.types';

/**
 * Generated class for the FeedbackModal component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'feedback-modal',
  templateUrl: 'feedback-modal.html'
})
export class FeedbackModalComponent {
  public feedbackForm: FormGroup;
  public disableFeedbackButton$: Observable<boolean>;
  public subscriptions: Subscription = new Subscription();

  public isUpdating$: Observable<boolean>;

  public loadingSpinner: Loading;

  constructor(
    formBuilder: FormBuilder,
    protected store: Store<fromRoot.State>,
    public viewCtrl: ViewController,
    public loadingCtrl: LoadingController
  ) {
      this.feedbackForm = formBuilder.group({
        navigationRating: ['', Validators.required],
        navigationComments: [''],
        featuresToAdd: [''],
        tipsForImporovement: [''],
      });

      this.disableFeedbackButton$ = this.feedbackForm.controls['navigationRating'].statusChanges
      .map(status => status !== 'VALID')
      .startWith(true);

      this.isUpdating$ = store.select(fromRoot.getProfileIsUpdating);
  }

  ionViewWillEnter() {
    const isUpdatingSub = this.isUpdating$
    .subscribe(this.handleUpdating.bind(this));

    this.subscriptions.add(isUpdatingSub);
  }

  ionViewWillLeave() {
    this.subscriptions.unsubscribe();
  }

  public setNavigationRating(rating: number) {
    this.feedbackForm.controls['navigationRating'].setValue(rating);
  }

  public sendFeedback() {
    const ratingInfo: IFeedbackRating = this.feedbackForm.value;
    this.store.dispatch(new profile.FeedbackAction(ratingInfo));
  }

  public handleUpdating(isUpdating: boolean): void {
    if (!isUpdating && this.loadingSpinner) {
      this.loadingSpinner.dismiss();
    } else if (isUpdating && !this.loadingSpinner) {
      this.loadingSpinner = this.loadingCtrl.create();
      this.loadingSpinner.present();
    }
  }
}
