# Digi Thermo Mobile App

## Quick Setup Instructions
- Clone the repo
- `npm install`
- `ionic serve`

Contributing to this repository followed the [Gitflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) throughout development.

## Development Stack
- [Angular](https://angular.io/) version "4.1.2"
- [Ionic version](https://ionicframework.com/) version "3.7.0"

## Dependencies
- [Firebase]() version "^4.1.3"

### Database
- [Firebase Angularfire 2 Database](https://github.com/angular/angularfire2/tree/master/src/database) version "^4.0.0-rc.1"
- [@ngrx library](https://github.com/ngrx) version "^1.2.0": 
    - [@ngrx/store](https://github.com/ngrx/store) version "^2.2.2"
    - [@ngrx/effects](https://github.com/ngrx/effects) version "^2.0.3"
    - [@ngrx/store-devtools](https://github.com/ngrx/store-devtools) (Get [Redux Devtools Extension](http://zalmoxisus.github.io/redux-devtools-extension)) version "^3.2.4"

### Authentication Integrations
- [Firebase Angularfire 2 AngularFireAuth](https://github.com/angular/angularfire2/tree/master/src/auth) (Github repo)
- [Ionic Facebook Login](https://ionicframework.com/docs/native/facebook/) version "^3.12.1"
    - Step by Step [Tutorial](https://ccoenraets.github.io/ionic-tutorial/ionic-facebook-integration.html)

### Unit Testing
- [Jasmine](https://jasmine.github.io/) version 2.5.2
- [Karma](https://karma-runner.github.io/2.0/index.html) version 1.4.1

## Locally Serving the Client App

### Setting up your environment
Start by installing the following command line applications:

- [Node](https://nodejs.org/en) 

- [Node Package Manager (NPM)](https://www.npmjs.com/) (Auto installs with Node) 

- [Angular CLI](https://cli.angular.io) 

_Note_: If you're on a Mac and use [Homebrew](https://brew.sh), then both [Node] & [NPM] are available

###Install Node & NPM using the Terminal
- visit this link: https://nodejs.org/en/, and click on the download button for the LTS version.

- Opening this file displays an installation wizard, bearing the following message: "This package will install node and npm into ..." 

- Click "Next" through the wizard (you have the option to adjust installation settings), until you get to the "Finish" page. 

- Verify that you have Node & NPM installed on your system.
```
$ node -v
$ npm -v
```
_Note_: If you see a version number, that means your installation was successful.

### Install the Angular CLI
In the Command Prompt, type the following command to install the Angular CLI:
```
$ npm install -g angular-cli
```

### Running Your Application Locally
After installing all of the System Dependencies, clone the project locally, & run the following command:
```
$ npm install
```
> This will build the node_modules necessary for running your app.

Run `ionic serve` for a dev server. Navigate to `http://localhost:8100/`. The app will automatically reload if you change any of the source files.
```
$ ionic serve
```

## Building to App Store

### iOS Build
For iOS the main build, and distribution onto iTunes Connect will be done through the .xcworkspace file that is generated under the `platforms` folder.
Prior to opening that file run the following command in terminal
`ionic prepare ios && ionic build ios`

### Build Android
```
- cordova build --release android

- jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore digithermo.keystore android-release-unsigned.apk digithermo

- /Users/NAMEHERE/Library/Android/sdk/build-tools/23.0.3/zipalign -v 4 android-release-unsigned.apk digithermo.apk
```

## Notes
This project is pointed to your firebase backend titled [digi-thermo-b459f](https://console.firebase.google.com/u/1/project/digi-thermo-b459f/overview) with the following configuration data:

```
<script src="https://www.gstatic.com/firebasejs/4.12.0/firebase.js"></script>
<script>
  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyB4at38akmpDoEgmZi-XrwD68oGrWSx3G8",
    authDomain: "digi-thermo-b459f.firebaseapp.com",
    databaseURL: "https://digi-thermo-b459f.firebaseio.com",
    projectId: "digi-thermo-b459f",
    storageBucket: "digi-thermo-b459f.appspot.com",
    messagingSenderId: "110354954103"
  };
  firebase.initializeApp(config);
</script>
```
